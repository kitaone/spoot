package app;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesValues {
	
	public static String geocodeApiKey;
	public static int defaultWorkLength;
	public static double startRandomLocLat;
	public static double startRandomLocLng;
	public static int RandomRadius;
	public static int randomClientNameLength;
	public static int serviceDuration;
	public static int clientWindowLength;
	public static String osrmAddress;
	public static int vehicleCapacity;
	public static int rideMaxStops;
	
	public void setProperties() {
		Properties prop = new Properties();
		InputStream input = null;

		try {
			InputStream inputStream = getClass().getClassLoader().getResourceAsStream("org/optaplanner/examples/common/config.properties");
			prop.load(inputStream);
			geocodeApiKey = prop.getProperty("geocodeApiKey");
			defaultWorkLength = Integer.parseInt(prop.getProperty("defaultWorkLength"));
			startRandomLocLat = Double.parseDouble(prop.getProperty("startRandomLocLat"));
			startRandomLocLng = Double.parseDouble(prop.getProperty("startRandomLocLng"));
			RandomRadius = Integer.parseInt(prop.getProperty("RandomRadius"));
			randomClientNameLength = Integer.parseInt(prop.getProperty("randomClientNameLength"));
			serviceDuration = Integer.parseInt(prop.getProperty("serviceDuration"));
			clientWindowLength = Integer.parseInt(prop.getProperty("clientWindowLength"));
			osrmAddress = prop.getProperty("osrmAddress");
			vehicleCapacity = Integer.parseInt(prop.getProperty("vehicleCapacity"));
			rideMaxStops = Integer.parseInt(prop.getProperty("rideMaxStops"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
