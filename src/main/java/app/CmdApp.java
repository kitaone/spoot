package app;

import java.io.IOException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.optaplanner.core.api.score.constraint.ConstraintMatchTotal;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.config.solver.termination.TerminationConfig;
import org.optaplanner.core.impl.phase.event.PhaseLifecycleListenerAdapter;
import org.optaplanner.core.impl.phase.scope.AbstractPhaseScope;
import org.optaplanner.core.impl.score.director.ScoreDirector;
import org.optaplanner.core.impl.solver.DefaultSolver;
import org.optaplanner.core.impl.solver.ProblemFactChange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import distance.utils.DistanceMatrixOSM;
import domain.Ride;
import domain.RoutingSolution;
import domain.Vehicle;
import domain.Visit;
import domain.VisitType;
import domain.location.Location;
import domain.location.RoadLocation;
import domain.time.TimeVisit;
import exporter.RoutingSolutionExport;
import importer.RoutingSolutionImporter;
import server.RideInformation;

public class CmdApp implements Runnable{
	
	protected static final Logger logger = LoggerFactory.getLogger(CmdApp.class);
	
	public SolverFactory<RoutingSolution> solverFactory;
	public Solver<RoutingSolution> solver;
	public RoutingSolution unsolvedRoutingSolution;
	public RoutingSolution solvedRoutingSolution;
	public ScoreDirector<RoutingSolution> scoreDirector;
	public List<ConstraintMatchTotal> constraintMatchTotalList;
	public int running = 0;
	public long insertingId = 10L;
	
	public long startWork;
	public long endWork;
	public long workLength;
	
	public double startingLat;
	public double startingLong;
	public double pickupLat;
	public double pickupLong;
	public String pickupName;
	public double deliveryLat;
	public double deliveryLong;
	public String deliveryName;
	public long firstDelLat;
	public long firstDelLong;
	public String pickupAddress;
	public String deliveryAddress;
	
	public boolean routeReady = false;
	
	public static void main(String[] args) throws IOException, InterruptedException {
		new CmdApp().start();
	}
	
	public void start() throws IOException, InterruptedException {
		running = 1;
		
		startWork = (LocalDateTime.now().getHour()*60 + LocalDateTime.now().getMinute()) * 1000;
		endWork = startWork + workLength * 1000;
		
		solverFactory = SolverFactory.createFromXmlResource("org/optaplanner/examples/vehiclerouting/solver/vehicleRoutingSolverConfig.xml");
        solver = solverFactory.buildSolver();
        
        unsolvedRoutingSolution = null;
        //unsolvedRoutingSolution = RoutingSolutionImporter.importSolution("startrealtw-n50-k1.vrp");
        //unsolvedRoutingSolution = RoutingSolutionImporter.createStartingSolution(startWork, endWork, 50.06133078, 19.89246272, 50.05545041, 19.9709129, 50.07830254, 19.97882276);
        unsolvedRoutingSolution = RoutingSolutionImporter.createStartingSolution(startWork, endWork, startingLat, startingLong, startingLat, startingLong, startingLat, startingLong, pickupName, deliveryName, pickupAddress, deliveryAddress);
        
        solvedRoutingSolution = solver.solve(unsolvedRoutingSolution);

        scoreDirector = solver.getScoreDirectorFactory().buildScoreDirector();
        scoreDirector.setWorkingSolution(solvedRoutingSolution);
        constraintMatchTotalList = new ArrayList<ConstraintMatchTotal>(scoreDirector.getConstraintMatchTotals());
        Collections.sort(constraintMatchTotalList);
 
        RoutingSolutionExport.exportSolutionToFile(solvedRoutingSolution, constraintMatchTotalList);
        running = -1;
	}
	
	public void waitForRide() throws IOException, InterruptedException {
		running = 1;
		startWork = (LocalDateTime.now().getHour()*60 + LocalDateTime.now().getMinute()) * 1000;
		endWork = startWork + workLength * 1000;

		solverFactory = SolverFactory.createFromXmlResource("org/optaplanner/examples/vehiclerouting/solver/vehicleRoutingSolverConfig.xml");
		TerminationConfig terminationConfig = new TerminationConfig();
		terminationConfig.setMinutesSpentLimit(workLength);
		solverFactory.getSolverConfig().setTerminationConfig(terminationConfig);
        solver = solverFactory.buildSolver();
        
        unsolvedRoutingSolution = null;
        unsolvedRoutingSolution = RoutingSolutionImporter.createStartingSolution(startWork, endWork, startingLat, startingLong, pickupLat, pickupLong, deliveryLat, deliveryLong, pickupName, deliveryName, pickupAddress, deliveryAddress);
        
		((DefaultSolver<RoutingSolution>) solver).addPhaseLifecycleListener(new PhaseLifecycleListenerAdapter<RoutingSolution>() {
			@Override
	        public void phaseEnded(AbstractPhaseScope<RoutingSolution> event) {
				//Uruchamiany tylko po fazie konstrukcynej
	            if(event.getLastCompletedStepScope().getPhaseScope().toString().equals("LocalSearchPhaseScope")) {
	            	Vehicle vehicle = event.getWorkingSolution().getVehicleList().get(0);
	            	Visit visit = vehicle.getNextVisit();
	            	//Przewijamy Visit do ostatniego w trasie
	            	while(visit.getNextVisit() != null) {
	            		visit = visit.getNextVisit();
	            	}
	            	//Jezeli poprzedni Visit jest zablokowany to blokujemy tez ostatni
	            	if(visit.getPreviousStandstill().getLocation().getLock()) {
	            		visit.getLocation().setLock(true);
	            	}
	            	//Po tej operacji do Local search trafia Solution gdzie odpowiednie lokalizacje sa zablokowane
	            }
	            routeReady = true;
	            String output = "";
	            logger.warn("Wyniki:");
            	for(Vehicle vehicle : event.getWorkingSolution().getVehicleList()) {
        			if(vehicle.getNextVisit() != null) {
        				TimeVisit visit = (TimeVisit) vehicle.getNextVisit();
        				while(visit != null) {
        					logger.warn(vehicle.getId() + " " + 
        							    RoutingSolutionExport.getVisitName(visit) + " " + 
        							    RoutingSolutionExport.getVisitName(visit.getRide().getPickupVisit()) + "->" + RoutingSolutionExport.getVisitName(visit.getRide().getDeliveryVisit()) + " " + 
        				                ((double) visit.getDistanceFromPreviousStandstill()/1000) + " " +
        				                RoutingSolutionExport.getHourMinutes(visit.getArrivalTime()) + " " +
        				                RoutingSolutionExport.getHourMinutes(visit.getDepartureTime()) + " " + 
        				                RoutingSolutionExport.getHourMinutes(visit.getReadyTime()) + " " +
        				                RoutingSolutionExport.getHourMinutes(visit.getDueTime()) + " " +
        				                visit.getLocation().getLock());
        					visit = visit.getNextVisit();
        				}
        			}
        			output += "\n";
        		}
            	//logger.warn("Wyniki:\n" + output);
	            
	        }
	    });
		
        solvedRoutingSolution = solver.solve(unsolvedRoutingSolution);
        
        scoreDirector = solver.getScoreDirectorFactory().buildScoreDirector();
        scoreDirector.setWorkingSolution(solvedRoutingSolution);
        constraintMatchTotalList = new ArrayList<ConstraintMatchTotal>(scoreDirector.getConstraintMatchTotals());
        Collections.sort(constraintMatchTotalList);
 
        RoutingSolutionExport.exportSolutionToFile(solvedRoutingSolution, constraintMatchTotalList);
        running = -1;
	}

	@Override
	public void run() {
		try {
			waitForRide();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void doProblemFactChange(ProblemFactChange<RoutingSolution> problemFactChange) {
		solver.addProblemFactChange(problemFactChange);
	}
	
	public void addRideInformation(RideInformation ride) {
		addRide(ride.getPickupName(), ride.getPickupLat(), ride.getPickupLong(), ride.getDeliveryName(), ride.getDeliveryLat(), ride.getDeliveryLong(), ride.getPickupAddress(), ride.getDeliveryAddress());	
	}
	
	public void addRide(String pickupName, double pickupLat, double pickupLong, String deliveryName, double deliveryLat, double deliveryLong, String pickupAddress, String deliveryAddress) {
		doProblemFactChange(new ProblemFactChange<RoutingSolution>() {
            public void doChange(ScoreDirector<RoutingSolution> scoreDirector) {
            	long startTime = (LocalDateTime.now().getHour()*60 + LocalDateTime.now().getMinute()) * 1000;
            	long endTime = startTime + (PropertiesValues.clientWindowLength * 1000);
            	
                RoutingSolution solution = (RoutingSolution) scoreDirector.getWorkingSolution();
                List<Location> locationList = solution.getLocationList();
                List<Visit> visitList = solution.getVisitList();
                List<Ride> rideList = solution.getRideList();
                
                //Oznaczenie ze jestesmy po fazie konstrukcynej 
                solution.getVehicleList().get(0).setAfterConst(true);
                
                RoadLocation pickupLocation = new RoadLocation();
                long pickupId = insertingId++;
                pickupLocation.setId(pickupId);
                pickupLocation.setName(pickupName);
                pickupLocation.setLatitude(pickupLat);
                pickupLocation.setLongitude(pickupLong);
                Map<RoadLocation, Double> travelPickupDistanceMap = new LinkedHashMap<RoadLocation, Double>(locationList.size()+2);
                
                RoadLocation deliveryLocation = new RoadLocation();
                long deliveryId = insertingId++;
                deliveryLocation.setId(deliveryId);
                deliveryLocation.setName(deliveryName);
                deliveryLocation.setLatitude(deliveryLat);
                deliveryLocation.setLongitude(deliveryLong);
                Map<RoadLocation, Double> travelDeliveryDistanceMap = new LinkedHashMap<RoadLocation, Double>(locationList.size()+2);
                
                try {
	                for (Location loc : locationList) {
	                	
	                	((RoadLocation) loc).getTravelDistanceMap().put(pickupLocation, DistanceMatrixOSM.generateDistance(loc, pickupLocation));
	                	travelPickupDistanceMap.put((RoadLocation) loc, DistanceMatrixOSM.generateDistance(pickupLocation, loc));
	                	((RoadLocation) loc).getTravelDistanceMap().put(deliveryLocation, DistanceMatrixOSM.generateDistance(loc, deliveryLocation));
	                	travelDeliveryDistanceMap.put((RoadLocation) loc, DistanceMatrixOSM.generateDistance(deliveryLocation, loc));
	                }
	                travelPickupDistanceMap.put(deliveryLocation, DistanceMatrixOSM.generateDistance(pickupLocation, deliveryLocation));
	                travelDeliveryDistanceMap.put(pickupLocation, DistanceMatrixOSM.generateDistance(deliveryLocation, pickupLocation));
		            
	                pickupLocation.setTravelDistanceMap(travelPickupDistanceMap);
		            deliveryLocation.setTravelDistanceMap(travelDeliveryDistanceMap);
		            
		            scoreDirector.beforeProblemFactAdded(pickupLocation);
	                solution.getLocationList().add(pickupLocation);
	                scoreDirector.afterProblemFactAdded(pickupLocation);
	                
		            scoreDirector.beforeProblemFactAdded(deliveryLocation);
	                solution.getLocationList().add(deliveryLocation);
	                scoreDirector.afterProblemFactAdded(deliveryLocation);
	                
	                //First pickup location
	                Visit pVisit = new TimeVisit();
	                pVisit.setId(pickupId);
	                pVisit.setLocation(pickupLocation);
	                TimeVisit timeWindowedCustomer = (TimeVisit) pVisit;
	                timeWindowedCustomer.setReadyTime(startTime);
	                timeWindowedCustomer.setDueTime(endTime);
	                timeWindowedCustomer.setServiceDuration(PropertiesValues.serviceDuration * 1000);
	                Ride ride = new Ride();
	                ride.setId(pickupId); // Reuse
	                ride.setRideName(pickupName + deliveryName);
	                ride.setSize(1);
	                pVisit.setRide(ride);
	                pVisit.setDemand(1);
	                pVisit.setMaxStopSize(PropertiesValues.rideMaxStops);
	                pVisit.setAddress(pickupAddress);
	                
	                //First pickup location
	                Visit dVisit = new TimeVisit();
	                dVisit.setId(deliveryId);
	                dVisit.setLocation(deliveryLocation);
	                timeWindowedCustomer = (TimeVisit) dVisit;
	                timeWindowedCustomer.setReadyTime(startTime);
	                timeWindowedCustomer.setDueTime(endTime);
	                timeWindowedCustomer.setServiceDuration(PropertiesValues.serviceDuration * 1000);
	                ride = pVisit.getRide();
	                dVisit.setRide(ride);
	                ride.setSize((ride.getSize() + 1 + 1) / 2);
	                dVisit.setDemand(-1);
	                dVisit.setMaxStopSize(PropertiesValues.rideMaxStops);
	                dVisit.setAddress(deliveryAddress);
	                
	                pVisit.setVisitType(VisitType.PICKUP);
	                ride.setPickupVisit(pVisit);
	                dVisit.setVisitType(VisitType.DELIVERY);
	                ride.setDeliveryVisit(dVisit);
	                                
		            scoreDirector.beforeProblemFactAdded(ride);
	                solution.getRideList().add(ride);
	                scoreDirector.afterProblemFactAdded(ride);
	                
		            scoreDirector.beforeProblemFactAdded(pVisit);
	                solution.getVisitList().add(pVisit);
	                scoreDirector.afterProblemFactAdded(pVisit);
	                
		            scoreDirector.beforeProblemFactAdded(dVisit);
	                solution.getVisitList().add(dVisit);
	                scoreDirector.afterProblemFactAdded(dVisit);
	                
                }catch(Exception e) {
                	System.out.println(e);
                }
            }
        });
	}
	
	//TODO
	public void removeRide(String pickupName, String deliveryName) {
		doProblemFactChange(new ProblemFactChange<RoutingSolution>() {
            public void doChange(ScoreDirector<RoutingSolution> scoreDirector) {
            	long startTime = (LocalDateTime.now().getHour()*60 + LocalDateTime.now().getMinute()) * 1000;
            	long endTime = startTime + (40 * 1000);
            	
                RoutingSolution solution = (RoutingSolution) scoreDirector.getWorkingSolution();
                List<Location> locationList = solution.getLocationList();
                List<Visit> visitList = solution.getVisitList();
                List<Ride> rideList = solution.getRideList();
                
                Ride rideToRemove = null;
                
                for (Ride ride : rideList) {
                	if (ride.getPickupVisit().getLocation().getName().equals(pickupName) && ride.getDeliveryVisit().getLocation().getName().equals(deliveryName)) {
                		rideToRemove = ride;
                		break;
                	}
                }                
            }
        });
	}
	
	public void lockLocation(String locationName) {
		for(Location loc : solver.getBestSolution().getLocationList()) {
			if (loc.getName().equals(locationName)) {
				loc.setLock(true);
				//loc.setDriverArrived(true);//
				break;
			}
		}
	}
	
	public void hideLocation(String locationName) {
		for(Location loc : solver.getBestSolution().getLocationList()) {
			if (loc.getName().equals(locationName)) {
				loc.setDriverArrived(true);
				break;
			}
		}
	}
	
	public String getClinetEta(String rideName) {
		String eta = "--:--";
		for(Ride ride : solver.getBestSolution().getRideList()) {
			if(ride.getRideName().equals(rideName)) {
				TimeVisit tVisit = (TimeVisit) ride.getPickupVisit();
				return RoutingSolutionExport.getHourMinutes(tVisit.getArrivalTime());
			}
		}
		return eta;
	}

}
