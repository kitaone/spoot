package domain.listeners;

import java.util.Objects;

import org.optaplanner.core.impl.domain.variable.listener.VariableListener;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import domain.Standstill;
import domain.Visit;

public class VisitIndexUpdatingVariableListener implements VariableListener<Visit> {

    @Override
    public void beforeEntityAdded(ScoreDirector scoreDirector, Visit visit) {
        // Do nothing
    }

    @Override
    public void afterEntityAdded(ScoreDirector scoreDirector, Visit visit) {
        updateVisitIndex(scoreDirector, visit);
    }

    @Override
    public void beforeVariableChanged(ScoreDirector scoreDirector, Visit visit) {
        // Do nothing
    }

    @Override
    public void afterVariableChanged(ScoreDirector scoreDirector, Visit visit) {
        updateVisitIndex(scoreDirector, visit);
    }

    @Override
    public void beforeEntityRemoved(ScoreDirector scoreDirector, Visit visit) {
        // Do nothing
    }

    @Override
    public void afterEntityRemoved(ScoreDirector scoreDirector, Visit visit) {
        // Do nothing
    }

    protected void updateVisitIndex(ScoreDirector scoreDirector, Visit sourceVisit) {
        Standstill previousStandstill = sourceVisit.getPreviousStandstill();
        Integer visitIndex;
        if (previousStandstill == null) {
            visitIndex = null;
        } else {
            visitIndex = previousStandstill.getVisitIndex();
            if (visitIndex != null) {
                visitIndex++;
            }
        }
        Visit shadowVisit = sourceVisit;
        while (shadowVisit != null && !Objects.equals(shadowVisit.getVisitIndex(), visitIndex)) {
            scoreDirector.beforeVariableChanged(shadowVisit, "visitIndex");
            shadowVisit.setVisitIndex(visitIndex);
            scoreDirector.afterVariableChanged(shadowVisit, "visitIndex");
            shadowVisit = shadowVisit.getNextVisit();
            if (visitIndex != null) {
                visitIndex++;
            }
        }
    }

}

