package domain.listeners;

import java.util.Objects;

import org.optaplanner.core.impl.domain.variable.listener.VariableListener;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import domain.Standstill;
import domain.Vehicle;
import domain.Visit;
import domain.time.TimeDepot;
import domain.time.TimeVisit;


public class ArrivalTimeUpdatingVariableListener implements VariableListener<Visit> {

 @Override
 public void beforeEntityAdded(ScoreDirector scoreDirector, Visit visit) {

 }

 @Override
 public void afterEntityAdded(ScoreDirector scoreDirector, Visit visit) {
     if (visit instanceof TimeVisit) {
         updateArrivalTime(scoreDirector, (TimeVisit) visit);
     }
 }

 @Override
 public void beforeVariableChanged(ScoreDirector scoreDirector, Visit visit) {

 }

 @Override
 public void afterVariableChanged(ScoreDirector scoreDirector, Visit visit) {
     if (visit instanceof TimeVisit) {
         updateArrivalTime(scoreDirector, (TimeVisit) visit);
     }
 }

 @Override
 public void beforeEntityRemoved(ScoreDirector scoreDirector, Visit visit) {

 }

 @Override
 public void afterEntityRemoved(ScoreDirector scoreDirector, Visit visit) {

 }

 protected void updateArrivalTime(ScoreDirector scoreDirector, TimeVisit sourceCustomer) {
     Standstill previousStandstill = sourceCustomer.getPreviousStandstill();
     Long departureTime;
     if (previousStandstill == null) {//jezeli nie ma prev to null
    	 departureTime = null;
     } else if (previousStandstill instanceof TimeVisit) {//jezeli prev to zwykla wizyta to czas odjazdu
    	 departureTime = ((TimeVisit) previousStandstill).getDepartureTime();
     } else {//jedyny inny przypadek kiedy prev jest pojazdem czyli depot
    	 departureTime = ((TimeDepot) ((Vehicle) previousStandstill).getDepot()).getReadyTime();
     }
     
     TimeVisit shadowCustomer = sourceCustomer;
     Long arrivalTime = calculateArrivalTime(shadowCustomer, departureTime);
     while (shadowCustomer != null && !Objects.equals(shadowCustomer.getArrivalTime(), arrivalTime)) {
    	 //sprawdzamy dopoki nastepna wizyta bedzie nullem lub dojdziemy do tego ktorego zmienialismy i jego czas sie zgadza
         scoreDirector.beforeVariableChanged(shadowCustomer, "arrivalTime");
         shadowCustomer.setArrivalTime(arrivalTime);
         scoreDirector.afterVariableChanged(shadowCustomer, "arrivalTime");
         departureTime = shadowCustomer.getDepartureTime();
         shadowCustomer = shadowCustomer.getNextVisit();
         arrivalTime = calculateArrivalTime(shadowCustomer, departureTime);
     }
 }

 private Long calculateArrivalTime(TimeVisit customer, Long previousDepartureTime) {
     if (customer == null || customer.getPreviousStandstill() == null) {//jezeli nie przydzielona wizyta to null
         return null;
     }
     if (customer.getPreviousStandstill() instanceof Vehicle) {//jezeli prev jest pojazdem to max(o ktorej mozna byc na wizycie, wyjazd z poprzedniej lok + czas przejazdu do lok)
         return Math.max(customer.getReadyTime(),
                 previousDepartureTime + customer.getDistanceFromPreviousStandstill());
     }
     return previousDepartureTime + customer.getDistanceFromPreviousStandstill();//wyjazd z poprzedniej lok + czas przejazdu do lok
 }

}

