package domain;

public enum VisitType {
    PICKUP,
    DELIVERY
}
