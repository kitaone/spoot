package domain;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.AnchorShadowVariable;
import org.optaplanner.core.api.domain.variable.CustomShadowVariable;
import org.optaplanner.core.api.domain.variable.PlanningVariable;
import org.optaplanner.core.api.domain.variable.PlanningVariableGraphType;
import org.optaplanner.core.api.domain.variable.PlanningVariableReference;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamInclude;

import domain.listeners.VisitIndexUpdatingVariableListener;
import domain.location.Location;
import domain.time.TimeVisit;

@PlanningEntity
@XStreamAlias("VrpVisit")
@XStreamInclude({
    TimeVisit.class
})
public class Visit extends AbstractPersistable implements Standstill {

    protected VisitType visitType;
    protected Location location;
    protected Ride ride;
    protected int maxStopSize;
    protected String address;

    //Zmienna planowana
    protected Standstill previousStandstill;

    //Shadow
    protected Visit nextVisit;
    protected Vehicle vehicle;
    protected Integer visitIndex;
    
    protected Integer demand;
    
    public void setAddress(String address) {
    	this.address = address;
    }
    
    public String getAddress() {
    	return address;
    }
    
    public void setMaxStopSize(int maxStopSize) {
    	this.maxStopSize = maxStopSize;
    }
    
    public int getMaxStopSize() {
    	return maxStopSize;
    }
    
    public void setDemand(Integer demand) {
    	this.demand = demand;
    }
    
    public Integer getDemand() {
    	return demand;
    }

    public VisitType getVisitType() {
        return visitType;
    }

    public void setVisitType(VisitType visitType) {
        this.visitType = visitType;
    }

    @Override
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getRideSize() {
        return ride.getSize();
    }

    public Ride getRide() {
        return ride;
    }

    public void setRide(Ride ride) {
        this.ride = ride;
    }

    @PlanningVariable(valueRangeProviderRefs = {"vehicleRange", "customerRange"},
            graphType = PlanningVariableGraphType.CHAINED)
    public Standstill getPreviousStandstill() {
        return previousStandstill;
    }

    public void setPreviousStandstill(Standstill previousStandstill) {
        this.previousStandstill = previousStandstill;
    }

    @Override
    public Visit getNextVisit() {
        return nextVisit;
    }

    @Override
    public void setNextVisit(Visit nextVisit) {
        this.nextVisit = nextVisit;
    }

    @Override
    @AnchorShadowVariable(sourceVariableName = "previousStandstill")
    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @CustomShadowVariable(variableListenerClass = VisitIndexUpdatingVariableListener.class,
            sources = {@PlanningVariableReference(variableName = "previousStandstill")})
    public Integer getVisitIndex() {
        return visitIndex;
    }

    public void setVisitIndex(Integer visitIndex) {
        this.visitIndex = visitIndex;
    }

    public long getDistanceFromPreviousStandstill() {
        if (previousStandstill == null) {
            throw new IllegalStateException("previousStandstill not initialized");
        }
        return getDistanceFrom(previousStandstill);
    }

    public long getDistanceFrom(Standstill standstill) {
        return standstill.getLocation().getDistanceTo(location);
    }


    public long getDistanceTo(Standstill standstill) {
        return location.getDistanceTo(standstill.getLocation());
    }

    @Override
    public String toString() {
        if (location.getName() == null) {
            return super.toString();
        }
        return location.getName();
    }

}
