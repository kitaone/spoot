package domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import domain.location.Location;

@XStreamAlias("VrpVehicle")
public class Vehicle extends AbstractPersistable implements Standstill {

    protected int capacity;
    protected Depot depot;
    protected boolean afterConst;

    //Shadow
    protected Visit nextVisit;

    public boolean getAfterConst() {
    	return afterConst;
    }
    
    public void setAfterConst(boolean afterConst) {
    	this.afterConst = afterConst;
    }
    
    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public Depot getDepot() {
        return depot;
    }

    public void setDepot(Depot depot) {
        this.depot = depot;
    }

    @Override
    public Visit getNextVisit() {
        return nextVisit;
    }

    @Override
    public void setNextVisit(Visit nextVisit) {
        this.nextVisit = nextVisit;
    }

    @Override
    public Vehicle getVehicle() {
        return this;
    }

    @Override
    public Integer getVisitIndex() {
        return 0;
    }

    @Override
    public Location getLocation() {
        return depot.getLocation();
    }

    public long getDistanceTo(Standstill standstill) {
        return depot.getDistanceTo(standstill);
    }

    @Override
    public String toString() {
        Location location = getLocation();
        if (location.getName() == null) {
            return super.toString();
        }
        return location.getName() + "/" + super.toString();
    }

}

