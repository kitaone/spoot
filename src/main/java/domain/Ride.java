package domain;

import org.optaplanner.core.api.domain.solution.cloner.DeepPlanningClone;

@DeepPlanningClone
public class Ride extends AbstractPersistable {

    protected Visit pickupVisit;
    protected Visit deliveryVisit;
    protected String rideName;

    protected int size;

    public String getRideName() {
    	return rideName;
    }
    
    public void setRideName(String rideName) {
    	this.rideName = rideName;
    }
    
    public Visit getPickupVisit() {
        return pickupVisit;
    }

    public void setPickupVisit(Visit pickupVisit) {
        this.pickupVisit = pickupVisit;
    }

    public Visit getDeliveryVisit() {
        return deliveryVisit;
    }

    public void setDeliveryVisit(Visit deliveryVisit) {
        this.deliveryVisit = deliveryVisit;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

}
