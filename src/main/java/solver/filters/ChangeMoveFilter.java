package solver.filters;

import org.optaplanner.core.impl.heuristic.selector.common.decorator.SelectionFilter;
import org.optaplanner.core.impl.heuristic.selector.move.generic.ChangeMove;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import domain.RoutingSolution;
import domain.Vehicle;
import domain.Visit;

public class ChangeMoveFilter implements SelectionFilter<ScoreDirector<RoutingSolution>, ChangeMove>{

	@Override
	public boolean accept(ScoreDirector<ScoreDirector<RoutingSolution>> scoreDirector, ChangeMove selection) {
		//Jezeli Visit ktory ma zostac gdzies przydzielony jest zablokowany
		Visit visit = (Visit) selection.getEntity();
		if(visit.getLocation().getLock()) {
			return false;
		}
		
		//Jezeli Visit do ktorej ma zostac przydzielony Visit jest zablokowany (to moze za bardzo obcinac)
		if(selection.getToPlanningValue() instanceof Visit && ((Visit) selection.getToPlanningValue()).getLocation().getLock()) {
			return false;
		}
		
		//Jezeli Visit przydzielany jest do Vehicle oraz pierwszy Visit po Vehicle jest zablokowany
		if(selection.getToPlanningValue() instanceof Vehicle && ((Vehicle) selection.getToPlanningValue()).getNextVisit().getLocation().getLock()) {
			return false;
		}
				
		return true;
	}

	
}
