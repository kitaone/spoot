package solver.filters;

import org.optaplanner.core.impl.heuristic.selector.common.decorator.SelectionFilter;
import org.optaplanner.core.impl.heuristic.selector.move.generic.ChangeMove;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import domain.RoutingSolution;
import domain.Vehicle;
import domain.Visit;

public class ConstructionFilter implements SelectionFilter<ScoreDirector<RoutingSolution>, ChangeMove>{

	@Override
	public boolean accept(ScoreDirector<ScoreDirector<RoutingSolution>> scoreDirector, ChangeMove selection) {
		//Jezeli przydzielamy do Vehicle po pierwszej fazie konstrukcyjnej
		if(selection.getToPlanningValue() instanceof Vehicle){
			if(((Vehicle) selection.getToPlanningValue()).getAfterConst()){
				return false;
			}
		}
		

		if(selection.getToPlanningValue() instanceof Visit){
			Visit visit = ((Visit) selection.getToPlanningValue());
			if(visit.getVehicle().getAfterConst()){
				if(visit.getNextVisit() != null){
					//Jezeli przydzielamy do Visit po pierwszej fazie konstrukcyjnej oraz nastepny Visit jest zablokowany
					if(visit.getNextVisit().getLocation().getLock()){
						return false;
					}
					//Jezeli przydzielamy do Visit po pierwszej fazie konstrukcyjnej oraz nastepny Visit jest ostatnim w trasie
					if(visit.getNextVisit().getNextVisit() == null) {
						return false;
					}
				}
			}
		}
		

		return true;
	}

}
