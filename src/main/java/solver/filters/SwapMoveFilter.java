package solver.filters;

import org.optaplanner.core.impl.heuristic.selector.common.decorator.SelectionFilter;
import org.optaplanner.core.impl.heuristic.selector.move.generic.ChangeMove;
import org.optaplanner.core.impl.heuristic.selector.move.generic.SwapMove;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import domain.RoutingSolution;
import domain.Visit;

public class SwapMoveFilter implements SelectionFilter<ScoreDirector<RoutingSolution>, SwapMove>{

	@Override
	public boolean accept(ScoreDirector<ScoreDirector<RoutingSolution>> scoreDirector, SwapMove selection) {
		if (selection.getLeftEntity() instanceof Visit && selection.getRightEntity() instanceof Visit) {
			Visit leftVisit = (Visit) selection.getLeftEntity();
			Visit rightVisit = (Visit) selection.getRightEntity();
			
			//Jezeli lewy albo prawy byl zablokowany
			if(leftVisit.getLocation().getLock() || rightVisit.getLocation().getLock()) {
				return false;
			}
		}
		return true;
	}

}
