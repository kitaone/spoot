package solver.filters;

import org.optaplanner.core.impl.heuristic.move.AbstractMove;
import org.optaplanner.core.impl.heuristic.selector.common.decorator.SelectionFilter;
import org.optaplanner.core.impl.heuristic.selector.move.generic.chained.SubChainChangeMove;
import org.optaplanner.core.impl.heuristic.selector.move.generic.chained.SubChainReversingChangeMove;
import org.optaplanner.core.impl.heuristic.selector.value.chained.SubChain;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import domain.RoutingSolution;
import domain.Vehicle;
import domain.Visit;

public class SubChainChangeMoveFilter implements SelectionFilter<ScoreDirector<RoutingSolution>, AbstractMove<RoutingSolution>>{

	@Override
	public boolean accept(ScoreDirector<ScoreDirector<RoutingSolution>> scoreDirector, AbstractMove<RoutingSolution> selection) {
		
		if(selection instanceof SubChainChangeMove) {
			SubChainChangeMove<RoutingSolution> move = (SubChainChangeMove<RoutingSolution>) selection;
			SubChain chain = move.getSubChain();
			
			//Przydzielany do Vehicle oraz pierwszy Visit jest zablokowany
			if(move.getToPlanningValue() instanceof Vehicle && ((Vehicle) move.getToPlanningValue()).getNextVisit().getLocation().getLock()) {
				return false;
			}
			
			//Przydzielany do Visit ktory jest zablokowany (to moze za bardzo obcinac)
			if(move.getToPlanningValue() instanceof Visit && ((Visit) move.getToPlanningValue()).getLocation().getLock()) {
				return false;
			}
			
			//W chain znajduje sie zablokowany Visit
			for(Object obj : chain.getEntityList()) {
				Visit visit = (Visit) obj;
				if(visit.getLocation().getLock()) {
					return false;
				}
	 		}
		} else if(selection instanceof SubChainReversingChangeMove) {
			SubChainReversingChangeMove<RoutingSolution> move = (SubChainReversingChangeMove<RoutingSolution>) selection;
			SubChain chain = move.getSubChain();
			
			//Przydzielany do Vehicle oraz pierwszy Visit jest zablokowany
			if(move.getToPlanningValue() instanceof Vehicle && ((Vehicle) move.getToPlanningValue()).getNextVisit().getLocation().getLock()) {
				return false;
			}
			
			//Przydzielany do Visit ktory jest zablokowany (to moze za bardzo obcinac)
			if(move.getToPlanningValue() instanceof Visit && ((Visit) move.getToPlanningValue()).getLocation().getLock()) {
				return false;
			}
			
			//W chain znajduje sie zablokowany Visit
			for(Object obj : chain.getEntityList()) {
				Visit visit = (Visit) obj;
				if(visit.getLocation().getLock()) {
					return false;
				}
	 		}
		}


		return true;
	}

}
