package solver.filters;

import org.optaplanner.core.impl.heuristic.move.AbstractMove;
import org.optaplanner.core.impl.heuristic.selector.common.decorator.SelectionFilter;
import org.optaplanner.core.impl.heuristic.selector.move.generic.chained.SubChainReversingSwapMove;
import org.optaplanner.core.impl.heuristic.selector.move.generic.chained.SubChainSwapMove;
import org.optaplanner.core.impl.heuristic.selector.value.chained.SubChain;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import domain.RoutingSolution;
import domain.Visit;

public class SubChainSwapMoveFilter implements SelectionFilter<ScoreDirector<RoutingSolution>, AbstractMove<RoutingSolution>>{

	@Override
	public boolean accept(ScoreDirector<ScoreDirector<RoutingSolution>> scoreDirector, AbstractMove<RoutingSolution> selection) {
		if(selection instanceof SubChainSwapMove) {
			SubChainSwapMove<RoutingSolution> move = (SubChainSwapMove<RoutingSolution>) selection;
			SubChain leftChain = move.getLeftSubChain();
			SubChain rightChain = move.getRightSubChain();
			
			//Jezeli lewy Chain posiada Visit z lockiem
			for(Object obj : leftChain.getEntityList()) {
				Visit visit = (Visit) obj;
				if(visit.getLocation().getLock()) {
					return false;
				}
			}
			
			//Jezeli prawy Chain posiada Visit z lockiem
			for(Object obj : rightChain.getEntityList()) {
				Visit visit = (Visit) obj;
				if(visit.getLocation().getLock()) {
					return false;
				}
			}
			
		}else if(selection instanceof SubChainReversingSwapMove) {
			SubChainReversingSwapMove<RoutingSolution> move = (SubChainReversingSwapMove<RoutingSolution>) selection;
			SubChain leftChain = move.getLeftSubChain();
			SubChain rightChain = move.getRightSubChain();
			
			//Jezeli lewy Chain posiada Visit z lockiem
			for(Object obj : leftChain.getEntityList()) {
				Visit visit = (Visit) obj;
				if(visit.getLocation().getLock()) {
					return false;
				}
			}
			
			//Jezeli prawy Chain posiada Visit z lockiem
			for(Object obj : rightChain.getEntityList()) {
				Visit visit = (Visit) obj;
				if(visit.getLocation().getLock()) {
					return false;
				}
			}
		}
		return true;
	}

}
