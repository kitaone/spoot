package solver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.optaplanner.core.impl.domain.solution.descriptor.SolutionDescriptor;
import org.optaplanner.core.impl.domain.variable.descriptor.GenuineVariableDescriptor;
import org.optaplanner.core.impl.domain.variable.inverserelation.SingletonInverseVariableDemand;
import org.optaplanner.core.impl.domain.variable.inverserelation.SingletonInverseVariableSupply;
import org.optaplanner.core.impl.heuristic.move.CompositeMove;
import org.optaplanner.core.impl.heuristic.move.Move;
import org.optaplanner.core.impl.heuristic.selector.move.factory.MoveIteratorFactory;
import org.optaplanner.core.impl.heuristic.selector.move.generic.chained.ChainedSwapMove;
import org.optaplanner.core.impl.score.director.InnerScoreDirector;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import domain.Ride;
import domain.RoutingSolution;
import domain.Visit;

public class RideSwapMoveIteratorFactory implements MoveIteratorFactory<RoutingSolution> {

    private List<GenuineVariableDescriptor<RoutingSolution>> variableDescriptorList = null;
    private List<SingletonInverseVariableSupply> inverseVariableSupplyList = null;

    @Override
    public long getSize(ScoreDirector<RoutingSolution> scoreDirector) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<? extends Move<RoutingSolution>> createOriginalMoveIterator(ScoreDirector<RoutingSolution> scoreDirector) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<? extends Move<RoutingSolution>> createRandomMoveIterator(
            ScoreDirector<RoutingSolution> scoreDirector, Random workingRandom) {
        if (inverseVariableSupplyList == null) {
            InnerScoreDirector<RoutingSolution> innerScoreDirector = (InnerScoreDirector<RoutingSolution>) scoreDirector;
            GenuineVariableDescriptor<RoutingSolution> variableDescriptor = innerScoreDirector.getSolutionDescriptor().findEntityDescriptorOrFail(Visit.class).getGenuineVariableDescriptor("previousStandstill");
            variableDescriptorList = Collections.singletonList(variableDescriptor);
            inverseVariableSupplyList = Collections.singletonList(innerScoreDirector.getSupplyManager().demand(new SingletonInverseVariableDemand(variableDescriptor)));
        }

        RoutingSolution solution = scoreDirector.getWorkingSolution();
        return new RideSwapMoveIterator(solution.getRideList(), workingRandom);
    }

    private class RideSwapMoveIterator implements Iterator<Move<RoutingSolution>> {

        private final List<Ride> rideList;
        private final Random workingRandom;

        public RideSwapMoveIterator(List<Ride> rideList, Random workingRandom) {
            this.rideList = rideList;
            this.workingRandom = workingRandom;
        }

        @Override
        public boolean hasNext() {
            return rideList.size() >= 2;
        }

        @Override
        public Move<RoutingSolution> next() {
            int leftRideIndex = workingRandom.nextInt(rideList.size());
            Ride leftRide = rideList.get(leftRideIndex);
            int rightRideIndex = workingRandom.nextInt(rideList.size() - 1);
            if (rightRideIndex >= leftRideIndex) {
                rightRideIndex++;
            }
            Ride rightRide = rideList.get(rightRideIndex);
        	
/*        	Ride leftRide = generateLeftRide();
        	while(leftRide.getPickupVisit().getLocation().getLock() || leftRide.getDeliveryVisit().getLocation().getLock()) {
        		System.out.println("generateLeftRide");
        		leftRide = generateLeftRide();
        	}
        	
        	Ride rightRide = generateRightRide();
        	while(rightRide.getPickupVisit().getLocation().getLock() || rightRide.getDeliveryVisit().getLocation().getLock()) {
        		System.out.println("generateRightRide");
        		rightRide = generateRightRide();
        	}*/

            return new CompositeMove<>(
                    new ChainedSwapMove<>(variableDescriptorList, inverseVariableSupplyList, leftRide.getPickupVisit(), rightRide.getPickupVisit()),
                    new ChainedSwapMove<>(variableDescriptorList, inverseVariableSupplyList, leftRide.getDeliveryVisit(), rightRide.getDeliveryVisit())
            );

        }
        
        public Ride generateLeftRide(){
        	Ride leftRide;
        	
            int leftRideIndex = workingRandom.nextInt(rideList.size());
            leftRide = rideList.get(leftRideIndex);
                    	
        	return leftRide;
        }
        
        public Ride generateRightRide(){
        	Ride rightRide;
        	
        	int rightRideIndex = workingRandom.nextInt(rideList.size() - 1);
            rightRide = rideList.get(rightRideIndex);
                    	
        	return rightRide;
        }

    }

}
