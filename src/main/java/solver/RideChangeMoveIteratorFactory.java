package solver;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.optaplanner.core.impl.domain.variable.descriptor.GenuineVariableDescriptor;
import org.optaplanner.core.impl.domain.variable.inverserelation.SingletonInverseVariableDemand;
import org.optaplanner.core.impl.domain.variable.inverserelation.SingletonInverseVariableSupply;
import org.optaplanner.core.impl.heuristic.move.CompositeMove;
import org.optaplanner.core.impl.heuristic.move.Move;
import org.optaplanner.core.impl.heuristic.move.NoChangeMove;
import org.optaplanner.core.impl.heuristic.selector.move.factory.MoveIteratorFactory;
import org.optaplanner.core.impl.heuristic.selector.move.generic.chained.ChainedChangeMove;
import org.optaplanner.core.impl.score.director.InnerScoreDirector;
import org.optaplanner.core.impl.score.director.ScoreDirector;
//import org.optaplanner.examples.vehiclerouting.domain.Ride;
//import org.optaplanner.examples.vehiclerouting.domain.Standstill;
//import org.optaplanner.examples.vehiclerouting.domain.VehicleRoutingSolution;
//import org.optaplanner.examples.vehiclerouting.domain.Visit;

import domain.Ride;
import domain.RoutingSolution;
import domain.Standstill;
import domain.Visit;

public class RideChangeMoveIteratorFactory implements MoveIteratorFactory<RoutingSolution> {

    private GenuineVariableDescriptor<RoutingSolution> variableDescriptor = null;
    private SingletonInverseVariableSupply inverseVariableSupply = null;

    @Override
    public long getSize(ScoreDirector<RoutingSolution> scoreDirector) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<? extends Move<RoutingSolution>> createOriginalMoveIterator(ScoreDirector<RoutingSolution> scoreDirector) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<? extends Move<RoutingSolution>> createRandomMoveIterator(
            ScoreDirector<RoutingSolution> scoreDirector, Random workingRandom) {
        if (inverseVariableSupply == null) {
            InnerScoreDirector<RoutingSolution> innerScoreDirector = (InnerScoreDirector<RoutingSolution>) scoreDirector;
            variableDescriptor = innerScoreDirector.getSolutionDescriptor().findEntityDescriptorOrFail(Visit.class).getGenuineVariableDescriptor("previousStandstill");
            inverseVariableSupply = innerScoreDirector.getSupplyManager().demand(new SingletonInverseVariableDemand(variableDescriptor));
        }

        RoutingSolution solution = scoreDirector.getWorkingSolution();
        List<Standstill> standstillList = new ArrayList<>(solution.getVehicleList().size() + solution.getVisitList().size());
        standstillList.addAll(solution.getVehicleList());
        standstillList.addAll(solution.getVisitList());
        return new RideChangeMoveIterator(solution.getRideList(), standstillList, workingRandom);
    }

    private class RideChangeMoveIterator implements Iterator<Move<RoutingSolution>> {

        private final List<Ride> rideList;
        private final List<Standstill> standstillList;
        private final Random workingRandom;

        public RideChangeMoveIterator(List<Ride> rideList, List<Standstill> standstillList, Random workingRandom) {
            this.rideList = rideList;
            this.standstillList = standstillList;
            this.workingRandom = workingRandom;
        }

        @Override
        public boolean hasNext() {
            return !rideList.isEmpty() && standstillList.size() >= 4;
        }

        @Override
        public Move<RoutingSolution> next() {
            Ride ride = rideList.get(workingRandom.nextInt(rideList.size()));
            Visit fromPickupVisit = ride.getPickupVisit();
            Visit fromDeliveryVisit = ride.getDeliveryVisit();
            Standstill toPickupVisit = standstillList.get(workingRandom.nextInt(standstillList.size()));
            List<Standstill> potentialToDeliveryVisitList = new ArrayList<>();
            potentialToDeliveryVisitList.add(fromPickupVisit);
            Visit visit = toPickupVisit.getNextVisit();
            while (visit != null) {
                if (visit != fromPickupVisit && visit != fromDeliveryVisit) {
                    potentialToDeliveryVisitList.add(visit);
                }
                visit = visit.getNextVisit();
            }
            Standstill toDeliveryVisit = potentialToDeliveryVisitList.get(workingRandom.nextInt(potentialToDeliveryVisitList.size()));
            ChainedChangeMove<RoutingSolution> pickupMove = new ChainedChangeMove<>(
                    fromPickupVisit, variableDescriptor, inverseVariableSupply, toPickupVisit);
            ChainedChangeMove<RoutingSolution> deliveryMove = new ChainedChangeMove<>(
                    fromDeliveryVisit, variableDescriptor, inverseVariableSupply, toDeliveryVisit);

            return new CompositeMove<>(pickupMove, deliveryMove);
        }

    }

}
