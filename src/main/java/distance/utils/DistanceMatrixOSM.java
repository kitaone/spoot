package distance.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

import app.PropertiesValues;
import domain.location.Location;
import domain.location.RoadLocation;

public class DistanceMatrixOSM {
	private static List<Location> locationList;

	public static void main(String[] args) throws IOException {
		locationList = new ArrayList<Location>();
		readLocations("matrix.txt");

		PrintWriter writer = new PrintWriter("matrix_output.txt");
		for (int i = 0 ; i < locationList.size() ; i++) {
			//String sUrl = "http://router.project-osrm.org/route/v1/driving/";
			String sUrl = PropertiesValues.osrmAddress;
			for (int j = 0 ; j < locationList.size() ; j++) {
				if (i!=j) {
					URL url = new URL(sUrl + locationList.get(i).getLongitude() + "," + locationList.get(i).getLatitude()+ ";" + locationList.get(j).getLongitude() + "," + locationList.get(j).getLatitude());
					HttpURLConnection con = (HttpURLConnection) url.openConnection();
					con.setRequestMethod("GET");
					
					InputStream in = new BufferedInputStream(con.getInputStream());
			        String response = convertStreamToString(in);
					
					JSONObject myObject = new JSONObject(response);
					JSONArray tablica = myObject.getJSONArray("routes");
					System.out.print(((JSONObject) tablica.get(0)).get("distance") + " ");
					writer.print(String.format("%.2f", Double.valueOf(((JSONObject) tablica.get(0)).get("distance").toString())/1000).replaceAll(",", ".") + " ");
				}else {
					System.out.print("0 ");
					writer.print("0 ");
				}
			}
			writer.println();
			System.out.println();
		}
		writer.close();
	}
	
    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
 
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
		
	public static void readLocations(String filename) throws FileNotFoundException {
		File file = new File(filename);
	      Scanner in = new Scanner(file);
	 
	      while(in.hasNextLine()) {
	    	  String line = in.nextLine();
	    	  Location location = new RoadLocation();
	    	  String[] tokens = line.split(",");
	    	  location.setLatitude(Double.valueOf(tokens[1]));
	    	  location.setLongitude(Double.valueOf(tokens[3]));
	    	  locationList.add(location);
	      }
	}
	
	public static double generateDistance(Location locationFrom, Location locationTo) throws IOException {
		//String sUrl = "http://router.project-osrm.org/route/v1/driving/";
		//String sUrl = "http://quantum2313.quantum.pl:5000/route/v1/driving/";
		String sUrl = PropertiesValues.osrmAddress;
		URL url = new URL(sUrl + locationFrom.getLongitude() + "," + locationFrom.getLatitude()+ ";" + locationTo.getLongitude() + "," + locationTo.getLatitude());
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		
		InputStream in = new BufferedInputStream(con.getInputStream());
        String response = convertStreamToString(in);
		
		JSONObject myObject = new JSONObject(response);
		JSONArray tablica = myObject.getJSONArray("routes");

		//writer.print(String.format("%.2f", Double.valueOf(((JSONObject) tablica.get(0)).get("distance").toString())/1000).replaceAll(",", ".") + " ");
		String str = String.format("%1.2f", Double.valueOf(((JSONObject) tablica.get(0)).get("distance").toString())/1000).replaceAll(",", ".");
		return Double.valueOf(str);
	}

}
