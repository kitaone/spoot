package distance.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DistanceMatrixElement;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;


import domain.location.Location;
import domain.location.RoadLocation;
import importer.ImportUtils;

public class DistanceMatrix {
	
	private static List<Location> locationList;

	public static void main(String[] args) throws ApiException, InterruptedException, IOException {
		locationList = new ArrayList<Location>();
		readLocations();
		GeoApiContext context = new GeoApiContext.Builder()
			    .apiKey("AIzaSyBiUUsWxQM_mZ6Qzelj7kaYJYC1d2rj8YA")
			    .build();
		
		LatLng[][] destinations = new LatLng[locationList.size()][locationList.size()-1];
		
		for(int i = 0 ; i < locationList.size() ; i++) {
			int counter = 0;
			for(int j = 0 ; j < locationList.size() ; j++) {
				if(i != j) {
					destinations[i][counter++] = new LatLng(locationList.get(j).getLatitude(), locationList.get(j).getLongitude());
				}
			}
		}
		PrintWriter writer = new PrintWriter("matrix_output.txt");

		int counter = 0;
		for(int i = 0 ; i < locationList.size() ; i++) {
			com.google.maps.model.DistanceMatrix matrix = DistanceMatrixApi.newRequest(context).origins(new LatLng(locationList.get(i).getLatitude(), locationList.get(i).getLongitude())).destinations(destinations[counter++]).await();
			int k = 0;
			for(int j = 0 ; j < locationList.size() ; j++) {
				if(i == j) {
					writer.print(0 + " ");
				}else {
					writer.print(matrix.rows[0].elements[k++].distance.inMeters/1000 + " ");
				}
			}
			writer.println();
		}
		writer.close();
		//matrix.rows[0].elements[0].distance
	}
	
	public static void readLocations() throws FileNotFoundException {
		File file = new File("matrix.txt");
	      Scanner in = new Scanner(file);
	 
	      while(in.hasNextLine()) {
	    	  String line = in.nextLine();
	    	  Location location = new RoadLocation();
	    	  String[] tokens = line.split(",");
	    	  location.setLatitude(Double.valueOf(tokens[1]));
	    	  location.setLongitude(Double.valueOf(tokens[3]));
	    	  locationList.add(location);
	      }
	}

}
