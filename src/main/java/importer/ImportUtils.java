package importer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import com.google.common.math.BigIntegerMath;

public class ImportUtils {
	
    protected static File inputFile;
    protected static BufferedReader bufferedReader;

    public static String readStringValue() throws IOException {
        return readStringValue("");
    }

    public static String readStringValue(String prefixRegex) throws IOException {
        return readStringValue(prefixRegex, "");
    }

    public static String readStringValue(String prefixRegex, String suffixRegex) throws IOException {
        String line = bufferedReader.readLine();
        if (line == null) {
            throw new IllegalArgumentException("File ends before a line is expected to contain an string value ("
                    + prefixRegex + "<value>" + suffixRegex + ").");
        }
        return removePrefixSuffixFromLine(line, prefixRegex, suffixRegex);
    }
    
    public static String removePrefixSuffixFromLine(String line, String prefixRegex, String suffixRegex) {
        String value = line.trim();
        if (!value.matches("^" + prefixRegex + ".*")) {
            throw new IllegalArgumentException("Read line (" + line + ") is expected to start with prefixRegex ("
                    + prefixRegex + ").");
        }
        value = value.replaceAll("^" + prefixRegex + "(.*)", "$1");
        if (!value.matches(".*" + suffixRegex + "$")) {
            throw new IllegalArgumentException("Read line (" + line + ") is expected to end with suffixRegex ("
                    + suffixRegex + ").");
        }
        value = value.replaceAll("(.*)" + suffixRegex + "$", "$1");
        value = value.trim();
        return value;
    }
    
    // ************************************************************************
    // Split methods
    // ************************************************************************

    public String[] splitBySpace(String line) {
        return splitBySpace(line, null);
    }

    public String[] splitBySpace(String line, Integer numberOfTokens) {
        return splitBy(line, "\\ ", "a space ( )", numberOfTokens, false, false);
    }

    public String[] splitBySpace(String line, Integer minimumNumberOfTokens, Integer maximumNumberOfTokens) {
        return splitBy(line, "\\ ", "a space ( )", minimumNumberOfTokens, maximumNumberOfTokens, false, false);
    }

    public String[] splitBySpace(String line, Integer minimumNumberOfTokens, Integer maximumNumberOfTokens,
            boolean trim, boolean removeQuotes) {
        return splitBy(line, "\\ ", "a space ( )", minimumNumberOfTokens, maximumNumberOfTokens, trim, removeQuotes);
    }

    public static String[] splitBySpacesOrTabs(String line) {
        return splitBySpacesOrTabs(line, null);
    }

    public static String[] splitBySpacesOrTabs(String line, Integer numberOfTokens) {
        return splitBy(line, "[\\ \\t]+", "spaces or tabs", numberOfTokens, false, false);
    }

    public static String[] splitBySpacesOrTabs(String line, Integer minimumNumberOfTokens, Integer maximumNumberOfTokens) {
        return splitBy(line, "[\\ \\t]+", "spaces or tabs", minimumNumberOfTokens, maximumNumberOfTokens,
                false, false);
    }

    public static String[] splitByPipelineAndTrim(String line, int numberOfTokens) {
        return splitBy(line, "\\|", "a pipeline (|)", numberOfTokens, true, false);
    }

    public static String[] splitBySemicolonSeparatedValue(String line) {
        return splitBy(line, ";", "a semicolon (;)", null, false, true);
    }

    public static String[] splitBySemicolonSeparatedValue(String line, int numberOfTokens) {
        return splitBy(line, ";", "a semicolon (;)", numberOfTokens, false, true);
    }

    public static String[] splitByCommaAndTrim(String line, int numberOfTokens) {
        return splitBy(line, "\\,", "a comma (,)", numberOfTokens, true, false);
    }

    public static String[] splitByCommaAndTrim(String line, Integer minimumNumberOfTokens, Integer maximumNumberOfTokens) {
        return splitBy(line, "\\,", "a comma (,)", minimumNumberOfTokens, maximumNumberOfTokens, true, false);
    }

    public static String[] splitBy(String line, String delimiterRegex, String delimiterName,
            Integer numberOfTokens, boolean trim, boolean removeQuotes) {
        return splitBy(line, delimiterRegex, delimiterName, numberOfTokens, numberOfTokens, trim, removeQuotes);
    }

    public static String[] splitBy(String line, String delimiterRegex, String delimiterName,
            Integer minimumNumberOfTokens, Integer maximumNumberOfTokens, boolean trim, boolean removeQuotes) {
        String[] lineTokens = line.split(delimiterRegex);
        if (removeQuotes) {
            List<String> lineTokenList = new ArrayList<>(lineTokens.length);
            for (int i = 0; i < lineTokens.length; i++) {
                String token = lineTokens[i];
                while ((trim ? token.trim() : token).startsWith("\"") && !(trim ? token.trim() : token).endsWith("\"")) {
                    i++;
                    if (i >= lineTokens.length) {
                        throw new IllegalArgumentException("The line (" + line
                                + ") has an invalid use of quotes (\").");
                    }
                    String delimiter;
                    switch (delimiterRegex) {
                        case "\\ ":
                            delimiter = " ";
                            break;
                        case "\\,":
                            delimiter = ",";
                            break;
                        default:
                            throw new IllegalArgumentException("Not supported delimiterRegex (" + delimiterRegex + ")");
                    }
                    token += delimiter + lineTokens[i];
                }
                if (trim) {
                    token = token.trim();
                }
                if (token.startsWith("\"") && token.endsWith("\"")) {
                    token = token.substring(1, token.length() - 1);
                    token = token.replaceAll("\"\"", "\"");
                }
                lineTokenList.add(token);
            }
            lineTokens = lineTokenList.toArray(new String[0]);
        }
        if (minimumNumberOfTokens != null && lineTokens.length < minimumNumberOfTokens) {
            throw new IllegalArgumentException("Read line (" + line + ") has " + lineTokens.length
                    + " tokens but is expected to contain at least " + minimumNumberOfTokens
                    + " tokens separated by " + delimiterName + ".");
        }
        if (maximumNumberOfTokens != null && lineTokens.length > maximumNumberOfTokens) {
            throw new IllegalArgumentException("Read line (" + line + ") has " + lineTokens.length
                    + " tokens but is expected to contain at most " + maximumNumberOfTokens
                    + " tokens separated by " + delimiterName + ".");
        }
        if (trim) {
            for (int i = 0; i < lineTokens.length; i++) {
                lineTokens[i] = lineTokens[i].trim();
            }
        }
        return lineTokens;
    }

    public static boolean parseBooleanFromNumber(String token) {
        switch (token) {
            case "0":
                return false;
            case "1":
                return true;
            default:
                throw new IllegalArgumentException("The token (" + token
                        + ") is expected to be 0 or 1 representing a boolean.");
        }
    }
    
    public static int readIntegerValue() throws IOException {
        return readIntegerValue("");
    }

    public static int readIntegerValue(String prefixRegex) throws IOException {
        return readIntegerValue(prefixRegex, "");
    }

    public static int readIntegerValue(String prefixRegex, String suffixRegex) throws IOException {
        String line = bufferedReader.readLine();
        if (line == null) {
            throw new IllegalArgumentException("File ends before a line is expected to contain an integer value ("
                    + prefixRegex + "<value>" + suffixRegex + ").");
        }
        String value = removePrefixSuffixFromLine(line, prefixRegex, suffixRegex);
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Read line (" + line + ") is expected to contain an integer value ("
                    + value + ").", e);
        }
    }

    public static long readLongValue() throws IOException {
        return readLongValue("");
    }

    public static long readLongValue(String prefixRegex) throws IOException {
        return readLongValue(prefixRegex, "");
    }

    public static long readLongValue(String prefixRegex, String suffixRegex) throws IOException {
        String line = bufferedReader.readLine();
        if (line == null) {
            throw new IllegalArgumentException("File ends before a line is expected to contain an integer value ("
                    + prefixRegex + "<value>" + suffixRegex + ").");
        }
        String value = removePrefixSuffixFromLine(line, prefixRegex, suffixRegex);
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Read line (" + line + ") is expected to contain an integer value ("
                    + value + ").", e);
        }
    }
    
    public static String readOptionalStringValue(String defaultValue) throws IOException {
        return readOptionalStringValue("", defaultValue);
    }

    public static String readOptionalStringValue(String prefixRegex, String defaultValue) throws IOException {
        return readOptionalStringValue(prefixRegex, "", defaultValue);
    }

    public static String readOptionalStringValue(String prefixRegex, String suffixRegex, String defaultValue) throws IOException {
        bufferedReader.mark(1024);
        boolean valid = true;
        String value = bufferedReader.readLine();
        if (value == null) {
            valid = false;
        } else {
            value = value.trim();
            if (value.matches("^" + prefixRegex + ".*")) {
                value = value.replaceAll("^" + prefixRegex + "(.*)", "$1");
            } else {
                valid = false;
            }
            if (value.matches(".*" + suffixRegex + "$")) {
                value = value.replaceAll("(.*)" + suffixRegex + "$", "$1");
            } else {
                valid = false;
            }
        }
        if (!valid) {
            bufferedReader.reset();
            return defaultValue;
        }
        value = value.trim();
        return value;
    }
    
    public static BigInteger factorial(int base) {
        BigInteger value = BigInteger.ONE;
        for (int i = 1; i <= base; i++) {
            value = value.multiply(BigInteger.valueOf(base));
        }
        return value;
    }
    
    public static String getFlooredPossibleSolutionSize(BigInteger possibleSolutionSize) {
        if (possibleSolutionSize.compareTo(BigInteger.valueOf(1000L)) < 0) {
            return possibleSolutionSize.toString();
        }
        return "10^" + (BigIntegerMath.log10(possibleSolutionSize, RoundingMode.FLOOR));
    }
    
    public static void readConstantLine(String constantRegex) throws IOException {
        String line = bufferedReader.readLine();
        if (line == null) {
            throw new IllegalArgumentException("File ends before a line is expected to be a constant regex ("
                    + constantRegex + ").");
        }
        String value = line.trim();
        if (!value.matches(constantRegex)) {
            throw new IllegalArgumentException("Read line (" + line + ") is expected to be a constant regex ("
                    + constantRegex + ").");
        }
    }
    
    public static void skipOptionalConstantLines(String constantRegex) throws IOException {
        boolean valid = true;
        while (valid) {
            valid = readOptionalConstantLine(constantRegex);
        }
    }
    
    public static boolean readOptionalConstantLine(String constantRegex) throws IOException {
        bufferedReader.mark(1024);
        boolean valid = true;
        String line = bufferedReader.readLine();
        if (line == null) {
            valid = false;
        } else {
            String value = line.trim();
            if (!value.matches(constantRegex)) {
                valid = false;
            }
        }
        if (!valid) {
            bufferedReader.reset();
        }
        return valid;
    }


    public static void readUntilConstantLine(String constantRegex) throws IOException {
        String line;
        String value;
        do {
            line = bufferedReader.readLine();
            if (line == null) {
                throw new IllegalArgumentException("File ends before a line is expected to be a constant regex ("
                        + constantRegex + ").");
            }
            value = line.trim();
        } while (!value.matches(constantRegex));
    }

}
