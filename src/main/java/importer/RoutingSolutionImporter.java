package importer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import app.PropertiesValues;
import distance.utils.DistanceMatrixOSM;
import domain.Depot;
import domain.Ride;
import domain.RoutingSolution;
import domain.Vehicle;
import domain.Visit;
import domain.VisitType;
import domain.location.DistanceType;
import domain.location.Location;
import domain.location.RoadLocation;
import domain.time.TimeDepot;
import domain.time.TimeRoutingSolution;
import domain.time.TimeVisit;

public class RoutingSolutionImporter {
	
    private static RoutingSolution solution;

    private static boolean timewindowed;
    private static int customerListSize;
    private static int vehicleListSize;
    private static int capacity;
    private static Map<Long, Location> locationMap;
    private static List<Depot> depotList;

	public static RoutingSolution importSolution(String inputfile) throws IOException {
		//File inputFile = new File("belgium-road-km-n50-k10.vrp");
		File inputFile = new File(inputfile);
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile), "UTF-8"));
		ImportUtils.inputFile = inputFile;
		ImportUtils.bufferedReader = reader;

        String firstLine = ImportUtils.readStringValue();
        if (firstLine.matches("\\s*NAME\\s*:.*")) {
            // Might be replaced by TimeWindowedVehicleRoutingSolution later on
            solution = new RoutingSolution();
            solution.setId(0L);
            solution.setName(ImportUtils.removePrefixSuffixFromLine(firstLine, "\\s*NAME\\s*:", ""));
            readVrpWebFormat();
        } else if (ImportUtils.splitBySpacesOrTabs(firstLine).length == 3) {
            timewindowed = false;
            solution = new RoutingSolution();
            solution.setId(0L);
            solution.setName("Test");
            String[] tokens = ImportUtils.splitBySpacesOrTabs(firstLine, 3);
            customerListSize = Integer.parseInt(tokens[0]);
            vehicleListSize = Integer.parseInt(tokens[1]);
            capacity = Integer.parseInt(tokens[2]);
            throw new UnsupportedOperationException();
//            readCourseraFormat();
        } else {
            timewindowed = true;
            solution = new TimeRoutingSolution();
            solution.setId(0L);
            solution.setName(firstLine);
            throw new UnsupportedOperationException();
//            readTimeWindowedFormat();
        }
        BigInteger possibleSolutionSize
                = ImportUtils.factorial(customerListSize + vehicleListSize - 1).divide(ImportUtils.factorial(vehicleListSize - 1));

        return solution;
    
	}
	
	public static void readVrpWebFormat() throws IOException {
        readVrpWebHeaders();
        readVrpWebLocationList();
        readVrpWebCustomerList();
        readVrpWebDepotList();
        createVrpWebVehicleList();
        ImportUtils.readConstantLine("EOF");
    }

    private static void readVrpWebHeaders() throws IOException {
    	ImportUtils.skipOptionalConstantLines("COMMENT *:.*");
        String vrpType = ImportUtils.readStringValue("TYPE *:");
        switch (vrpType) {
            case "CVRP":
                timewindowed = false;
                break;
            case "CVRPTW":
                timewindowed = true;
                Long solutionId = solution.getId();
                String solutionName = solution.getName();
                solution = new TimeRoutingSolution();
                solution.setId(solutionId);
                solution.setName(solutionName);
                break;
            default:
                throw new IllegalArgumentException("The vrpType (" + vrpType + ") is not supported.");
        }
        customerListSize = ImportUtils.readIntegerValue("DIMENSION *:");
        String edgeWeightType = ImportUtils.readStringValue("EDGE_WEIGHT_TYPE *:");
        if (edgeWeightType.equalsIgnoreCase("EUC_2D")) {
            solution.setDistanceType(DistanceType.AIR_DISTANCE);
        } else if (edgeWeightType.equalsIgnoreCase("EXPLICIT")) {
            solution.setDistanceType(DistanceType.ROAD_DISTANCE);
            String edgeWeightFormat = ImportUtils.readStringValue("EDGE_WEIGHT_FORMAT *:");
            if (!edgeWeightFormat.equalsIgnoreCase("FULL_MATRIX")) {
                throw new IllegalArgumentException("The edgeWeightFormat (" + edgeWeightFormat + ") is not supported.");
            }
        } else if (edgeWeightType.equalsIgnoreCase("SEGMENTED_EXPLICIT")) {
            solution.setDistanceType(DistanceType.SEGMENTED_ROAD_DISTANCE);
            String edgeWeightFormat = ImportUtils.readStringValue("EDGE_WEIGHT_FORMAT *:");
            if (!edgeWeightFormat.equalsIgnoreCase("HUB_AND_NEARBY_MATRIX")) {
                throw new IllegalArgumentException("The edgeWeightFormat (" + edgeWeightFormat + ") is not supported.");
            }
        } else {
            throw new IllegalArgumentException("The edgeWeightType (" + edgeWeightType + ") is not supported.");
        }
        solution.setDistanceUnitOfMeasurement(ImportUtils.readOptionalStringValue("EDGE_WEIGHT_UNIT_OF_MEASUREMENT *:", "distance"));
        capacity = ImportUtils.readIntegerValue("CAPACITY *:");
    }

    private static void readVrpWebLocationList() throws IOException {
        DistanceType distanceType = solution.getDistanceType();
        List<RoadLocation> hubLocationList = null;
        locationMap = new LinkedHashMap<>(customerListSize);
        if (distanceType == DistanceType.SEGMENTED_ROAD_DISTANCE) {
            int hubListSize = ImportUtils.readIntegerValue("HUBS *:");
            hubLocationList = new ArrayList<>(hubListSize);
            ImportUtils.readConstantLine("HUB_COORD_SECTION");
            for (int i = 0; i < hubListSize; i++) {
                String line = ImportUtils.bufferedReader.readLine();
                String[] lineTokens = ImportUtils.splitBySpacesOrTabs(line.trim(), 3, 4);
                RoadLocation location = new RoadLocation();
                location.setId(Long.parseLong(lineTokens[0]));
                location.setLatitude(Double.parseDouble(lineTokens[1]));
                location.setLongitude(Double.parseDouble(lineTokens[2]));
                if (lineTokens.length >= 4) {
                    location.setName(lineTokens[3]);
                }
                hubLocationList.add(location);
                locationMap.put(location.getId(), location);
            }
        }
        List<Location> customerLocationList = new ArrayList<>(customerListSize);
        ImportUtils.readConstantLine("NODE_COORD_SECTION");
        for (int i = 0; i < customerListSize; i++) {
            String line = ImportUtils.bufferedReader.readLine();
            String[] lineTokens = ImportUtils.splitBySpacesOrTabs(line.trim(), 3, 4);
            Location location;
            location = new RoadLocation();
            location.setId(Long.parseLong(lineTokens[0]));
            location.setLatitude(Double.parseDouble(lineTokens[1]));
            location.setLongitude(Double.parseDouble(lineTokens[2]));
            if (lineTokens.length >= 4) {
                location.setName(lineTokens[3]);
            }
            customerLocationList.add(location);
            locationMap.put(location.getId(), location);
        }
        if (distanceType == DistanceType.ROAD_DISTANCE) {
        	ImportUtils.readConstantLine("EDGE_WEIGHT_SECTION");
            for (int i = 0; i < customerListSize; i++) {
                RoadLocation location = (RoadLocation) customerLocationList.get(i);
                Map<RoadLocation, Double> travelDistanceMap = new LinkedHashMap<>(customerListSize);
                String line = ImportUtils.bufferedReader.readLine();
                String[] lineTokens = ImportUtils.splitBySpacesOrTabs(line.trim(), customerListSize);
                for (int j = 0; j < customerListSize; j++) {
                    double travelDistance = Double.parseDouble(lineTokens[j]);
                    if (i == j) {
                        if (travelDistance != 0.0) {
                            throw new IllegalStateException("The travelDistance (" + travelDistance
                                    + ") should be zero.");
                        }
                    } else {
                        RoadLocation otherLocation = (RoadLocation) customerLocationList.get(j);
                        travelDistanceMap.put(otherLocation, travelDistance);
                    }
                }
                location.setTravelDistanceMap(travelDistanceMap);
            }
        }
        List<Location> locationList;
        if (distanceType == DistanceType.SEGMENTED_ROAD_DISTANCE) {
            locationList = new ArrayList<>(hubLocationList.size() + customerListSize);
            locationList.addAll(hubLocationList);
            locationList.addAll(customerLocationList);
        } else {
            locationList = customerLocationList;
        }
        solution.setLocationList(locationList);
    }

    private static void readVrpWebCustomerList() throws IOException {
    	ImportUtils.readConstantLine("DEMAND_SECTION");
        depotList = new ArrayList<>(customerListSize);
        List<Visit> visitList = new ArrayList<>(customerListSize);
        List<Ride> rideList = new ArrayList<>((customerListSize + 1) / 2);
        Visit pickupVisit = null;
        for (int i = 0; i < customerListSize; i++) {
            String line = ImportUtils.bufferedReader.readLine();
            String[] lineTokens = ImportUtils.splitBySpacesOrTabs(line.trim(), timewindowed ? 5 : 2);
            long id = Long.parseLong(lineTokens[0]);
            int demand = Integer.parseInt(lineTokens[1]);
            // Depots have no demand
            if (demand == 0) {
                Depot depot = timewindowed ? new TimeDepot() : new Depot();
                depot.setId(id);
                Location location = locationMap.get(id);
                if (location == null) {
                    throw new IllegalArgumentException("The depot with id (" + id
                            + ") has no location (" + location + ").");
                }
                depot.setLocation(location);
                if (timewindowed) {
                    TimeDepot timeWindowedDepot = (TimeDepot) depot;
                    timeWindowedDepot.setReadyTime(Long.parseLong(lineTokens[2]));
                    timeWindowedDepot.setDueTime(Long.parseLong(lineTokens[3]));
                    long serviceDuration = Long.parseLong(lineTokens[4]);
                    if (serviceDuration != 0L) {
                        throw new IllegalArgumentException("The depot with id (" + id
                                + ") has a serviceDuration (" + serviceDuration + ") that is not 0.");
                    }
                }
                depotList.add(depot);
            } else {
                Visit visit = timewindowed ? new TimeVisit() : new Visit();
                visit.setId(id);
                Location location = locationMap.get(id);
                if (location == null) {
                    throw new IllegalArgumentException("The visit with id (" + id
                            + ") has no location (" + location + ").");
                }
                visit.setLocation(location);
                if (timewindowed) {
                    TimeVisit timeWindowedCustomer = (TimeVisit) visit;
                    timeWindowedCustomer.setReadyTime(Long.parseLong(lineTokens[2]));
                    timeWindowedCustomer.setDueTime(Long.parseLong(lineTokens[3]));
                    timeWindowedCustomer.setServiceDuration(Long.parseLong(lineTokens[4]));
                }
                if (pickupVisit == null) {
                    Ride ride = new Ride();
                    ride.setId(id); // Reuse
                    ride.setSize(demand);
                    visit.setRide(ride);
                    pickupVisit = visit;
                    pickupVisit.setDemand(demand);
                } else {
                    Ride ride = pickupVisit.getRide();
                    visit.setRide(ride);
                    ride.setSize((ride.getSize() + demand + 1) / 2);
                    Visit deliveryVisit;
                    if (timewindowed
                            && ((TimeVisit) pickupVisit).getDueTime() > ((TimeVisit) visit).getDueTime()) {
                        deliveryVisit = pickupVisit;
                        pickupVisit = visit;
                    } else {
                        deliveryVisit = visit;
                    }
                    pickupVisit.setVisitType(VisitType.PICKUP);
                    ride.setPickupVisit(pickupVisit);
                    deliveryVisit.setDemand(-demand);
                    deliveryVisit.setVisitType(VisitType.DELIVERY);
                    ride.setDeliveryVisit(deliveryVisit);
                    // with uneven customers, only add ride and visits if there is a pickup and delivery
                    rideList.add(ride);
                    visitList.add(pickupVisit);
                    visitList.add(deliveryVisit);
                    pickupVisit = null;
                }

            }
        }
        solution.setRideList(rideList);
        solution.setVisitList(visitList);
        solution.setDepotList(depotList);
    }

    private static void readVrpWebDepotList() throws IOException {
    	ImportUtils.readConstantLine("DEPOT_SECTION");
        int depotCount = 0;
        long id = ImportUtils.readLongValue();
        while (id != -1) {
            depotCount++;
            id = ImportUtils.readLongValue();
        }
        if (depotCount != depotList.size()) {
            throw new IllegalStateException("The number of demands with 0 demand (" + depotList.size()
                    + ") differs from the number of depots (" + depotCount + ").");
        }
    }

    private static void createVrpWebVehicleList() throws IOException {
        String inputFileName = ImportUtils.inputFile.getName();
        if (inputFileName.toLowerCase().startsWith("tutorial")) {
            vehicleListSize = ImportUtils.readIntegerValue("VEHICLES *:");
        } else {
            String inputFileNameRegex = "^.+\\-k(\\d+)\\.vrp$";
            if (!inputFileName.matches(inputFileNameRegex)) {
                throw new IllegalArgumentException("The inputFileName (" + inputFileName
                        + ") does not match the inputFileNameRegex (" + inputFileNameRegex + ").");
            }
            String vehicleListSizeString = inputFileName.replaceAll(inputFileNameRegex, "$1");
            try {
                vehicleListSize = Integer.parseInt(vehicleListSizeString);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("The inputFileName (" + inputFileName
                        + ") has a vehicleListSizeString (" + vehicleListSizeString + ") that is not a number.", e);
            }
        }
        createVehicleList();
    }

    private static void createVehicleList() {
        List<Vehicle> vehicleList = new ArrayList<>(vehicleListSize);
        long id = 0;
        for (int i = 0; i < vehicleListSize; i++) {
            Vehicle vehicle = new Vehicle();
            vehicle.setId(id);
            id++;
            vehicle.setCapacity(capacity);
            // Round robin the vehicles to a depot if there are multiple depots
            vehicle.setDepot(depotList.get(i % depotList.size()));
            vehicleList.add(vehicle);
        }
        solution.setVehicleList(vehicleList);
    }

	public static RoutingSolution createStartingSolution(long start, long end, double startLat, double startLong, double pickupLat, double pickupLong, double deliveryLat, double deliveryLong, String pickupName, String deliveryName, String pickupAddress, String deliveryAddress) throws IOException, InterruptedException {
		solution = new TimeRoutingSolution();
        solution.setId(0L);
        solution.setName("StartingSolution");
        solution.setDistanceType(DistanceType.ROAD_DISTANCE);
        solution.setDistanceUnitOfMeasurement("km");
        customerListSize = 3;
        locationMap = new LinkedHashMap<>(customerListSize);
        List<Location> customerLocationList = new ArrayList<>(customerListSize);
        
        //Starting location
        Location location;
        location = new RoadLocation();
        location.setId(0L);
        location.setLatitude(startLat);
        location.setLongitude(startLong);
        location.setName("StartingLocation");
        customerLocationList.add(location);
        locationMap.put(location.getId(), location);
        
        //First pickup location
        location = new RoadLocation();
        location.setId(1L);
        location.setLatitude(pickupLat);
        location.setLongitude(pickupLong);
        location.setName(pickupName);
        customerLocationList.add(location);
        locationMap.put(location.getId(), location);
        
        //First delivery location
        location = new RoadLocation();
        location.setId(2L);
        location.setLatitude(deliveryLat);
        location.setLongitude(deliveryLong);
        location.setName(deliveryName);
        customerLocationList.add(location);
        locationMap.put(location.getId(), location);
        
        //Distance matrix
        for (int i = 0; i < customerListSize; i++) {
            RoadLocation roadLocation = (RoadLocation) customerLocationList.get(i);
            Map<RoadLocation, Double> travelDistanceMap = new LinkedHashMap<>(customerListSize);
            for (int j = 0; j < customerListSize; j++) {
            	Thread.sleep(500);
                double travelDistance = DistanceMatrixOSM.generateDistance(customerLocationList.get(i), customerLocationList.get(j));
                if (i == j) {
                    if (travelDistance != 0.0) {
                        throw new IllegalStateException("The travelDistance (" + travelDistance
                                + ") should be zero.");
                    }
                } else {
                    RoadLocation otherLocation = (RoadLocation) customerLocationList.get(j);
                    travelDistanceMap.put(otherLocation, travelDistance);
                }
            }
            roadLocation.setTravelDistanceMap(travelDistanceMap);
        }
        solution.setLocationList(customerLocationList);
        
        //Demand
        depotList = new ArrayList<>(customerListSize);
        List<Visit> visitList = new ArrayList<>(customerListSize);
        List<Ride> rideList = new ArrayList<>((customerListSize + 1) / 2);
        Visit pickupVisit = null;
        
        //Starting demand
        Depot depot = new TimeDepot();
        depot.setId(0L);
        location = null;
        location = locationMap.get(0L);
        depot.setLocation(location);
        TimeDepot timeWindowedDepot = (TimeDepot) depot;
        timeWindowedDepot.setReadyTime(start);
        timeWindowedDepot.setDueTime(end);
        depotList.add(depot);
        
        //First pickup location
        Visit visit = new TimeVisit();
        visit.setId(1L);
        location = null;
        location = locationMap.get(1L);
        visit.setLocation(location);
        TimeVisit timeWindowedCustomer = (TimeVisit) visit;
        timeWindowedCustomer.setReadyTime(start);
        timeWindowedCustomer.setDueTime(start + PropertiesValues.clientWindowLength * 1000);
        timeWindowedCustomer.setServiceDuration(PropertiesValues.serviceDuration * 1000);
        Ride ride = new Ride();
        ride.setId(1L); // Reuse
        ride.setSize(1);
        visit.setRide(ride);
        pickupVisit = visit;
        pickupVisit.setDemand(1);
        pickupVisit.setMaxStopSize(PropertiesValues.rideMaxStops);
        pickupVisit.setAddress(pickupAddress);
        
        //First pickup location
        visit = new TimeVisit();
        visit.setId(2L);
        location = null;
        location = locationMap.get(2L);
        visit.setLocation(location);
        timeWindowedCustomer = (TimeVisit) visit;
        timeWindowedCustomer.setReadyTime(start);
        timeWindowedCustomer.setDueTime(end);
        timeWindowedCustomer.setServiceDuration(PropertiesValues.serviceDuration * 1000);
        ride = pickupVisit.getRide();
        visit.setRide(ride);
        ride.setRideName(pickupName + deliveryName);
        ride.setSize((ride.getSize() + 1 + 1) / 2);
        Visit deliveryVisit;
        deliveryVisit = visit;

        pickupVisit.setVisitType(VisitType.PICKUP);
        ride.setPickupVisit(pickupVisit);
        deliveryVisit.setDemand(-1);
        deliveryVisit.setMaxStopSize(PropertiesValues.rideMaxStops);
        deliveryVisit.setAddress(deliveryAddress);
        deliveryVisit.setVisitType(VisitType.DELIVERY);
        ride.setDeliveryVisit(deliveryVisit);
        
        rideList.add(ride);
        visitList.add(pickupVisit);
        visitList.add(deliveryVisit);
        pickupVisit = null;
        
        solution.setRideList(rideList);
        solution.setVisitList(visitList);
        solution.setDepotList(depotList);
        
        //Vehilces
        vehicleListSize = 1;
        List<Vehicle> vehicleList = new ArrayList<>(vehicleListSize);
        Vehicle vehicle = new Vehicle();
        vehicle.setId(1L);
        vehicle.setCapacity(PropertiesValues.vehicleCapacity);
        // Round robin the vehicles to a depot if there are multiple depots
        vehicle.setDepot(depotList.get(0));
        vehicleList.add(vehicle);

        solution.setVehicleList(vehicleList);
        
		return solution;
	}


}
