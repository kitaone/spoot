package exporter;


public class SolutionLine {
    private final String vehicleId;
    private final String visitName;
    private final String fromTo;
    private final String distance;
    private final String arrival;
    private final String departure;
    private final String ready;
    private final String due;
    

	public SolutionLine(String vehicleId, String visitName, String fromTo,
			String distance, String arrival, String departure,
			String ready, String due) {
		super();
		this.vehicleId = vehicleId;
		this.visitName = visitName;
		this.fromTo = fromTo;
		this.distance = distance;
		this.arrival = arrival;
		this.departure = departure;
		this.ready = ready;
		this.due = due;
	}
	public String getVehicleId() {
		return vehicleId;
	}
	public String getVisitName() {
		return visitName;
	}
	public String getFromTo() {
		return fromTo;
	}
	public String getDistance() {
		return distance;
	}
	public String getArrival() {
		return arrival;
	}
	public String getDeparture() {
		return departure;
	}
	public String getReady() {
		return ready;
	}
	public String getDue() {
		return due;
	}
}
