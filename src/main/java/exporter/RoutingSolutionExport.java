package exporter;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.optaplanner.core.api.score.constraint.ConstraintMatch;
import org.optaplanner.core.api.score.constraint.ConstraintMatchTotal;

import domain.RoutingSolution;
import domain.Vehicle;
import domain.Visit;
import domain.time.TimeVisit;

public class RoutingSolutionExport {
	
	private static RoutingSolution solution;

	public static void exportSolutionToFile(RoutingSolution sol, List<ConstraintMatchTotal> constraintList) throws FileNotFoundException {
		solution = sol;
		
		//PrintWriter out = new PrintWriter("solved.txt");
		PrintWriter out = new PrintWriter(System.getProperty("java.io.tmpdir") + "//solved.txt");
		///BufferedWriter bw = new BufferedWriter(new FileWriter(System.getProperty("java.io.tmpdir") + "//TMS_DATA.VRP",false));
		List<Vehicle> vehicleList = solution.getVehicleList();
		
		for(Vehicle vehicle : vehicleList) {
			if(vehicle.getNextVisit() != null) {
				TimeVisit visit = (TimeVisit) vehicle.getNextVisit();
				while(visit != null) {
					out.println(vehicle.getId() + " " + 
				                getVisitName(visit) + " " + 
							    getVisitName(visit.getRide().getPickupVisit()) + "->" + getVisitName(visit.getRide().getDeliveryVisit()) + " " + 
				                ((double) visit.getDistanceFromPreviousStandstill()/1000) + " " +
				                getHourMinutes(visit.getArrivalTime()) + " " +
				                getHourMinutes(visit.getDepartureTime()) + " " + 
				                getHourMinutes(visit.getReadyTime()) + " " +
				                getHourMinutes(visit.getDueTime()));
					visit = visit.getNextVisit();
				}
			}
			out.println();
		}
		printBrokenConstraint(constraintList, out);
		
		out.close();
	}
	
	public static String getHourMinutes(long time) {
		time = time / 1000;
		long hours = TimeUnit.MINUTES.toHours(time);
		long remainMinute = time - TimeUnit.HOURS.toMinutes(hours);
		String result = String.format("%02d", hours) + ":" + String.format("%02d", remainMinute);
		return result;
	}
	
	public static String getVisitName(Visit visit) {
		return visit.getLocation().getName();
	}
	
	public static void printBrokenConstraint(List<ConstraintMatchTotal> constraintList, PrintWriter out) {
		for(ConstraintMatchTotal constraint : constraintList) {
			out.println(constraint.getConstraintName() + " " + constraint.getConstraintMatchCount() + " " + constraint.getScoreTotal());
			for(ConstraintMatch item : constraint.getConstraintMatchSet()) {
				out.println(item.getJustificationList() + " " + item.getScore());
			}
		}
		
	}

}
