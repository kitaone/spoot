package server;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import app.PropertiesValues;

@ApplicationPath("")
public class ServerApplication extends Application {
	private Set<Object> singletons = new HashSet<Object>();

	public ServerApplication() {
		PropertiesValues prop = new PropertiesValues();
		prop.setProperties();
		singletons.add(new AppServiceImpl());
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}
