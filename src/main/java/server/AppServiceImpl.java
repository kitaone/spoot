package server;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;

import app.CmdApp;
import app.PropertiesValues;
import domain.RoutingSolution;
import domain.Visit;
import domain.VisitType;
import domain.location.LatLng;


@Path("/spoot")
@Produces("application/json")
public class AppServiceImpl implements AppService {
	
	protected static final Logger logger = LoggerFactory.getLogger(AppServiceImpl.class);
	
	public static Thread planning;
	public CmdApp planningObj/* = new CmdApp()*/;
	public static int waitingToStart = 0;
	public List<RideInformation> pendingRides = new ArrayList<RideInformation>();
	public List<RideInformation> approvedRides = new ArrayList<RideInformation>();
	public List<RideInformation> cancelRides = new ArrayList<RideInformation>();
	public int rideNumber = 2;
	

	@Override
	@GET
    @Path("/areualive")
	public SpootResponse areUAlive() {
		SpootResponse response = new SpootResponse();
		response.setInfo("areualive");
		response.setContent("Alive | 0.1");
		response.setCode(0);
		//System.out.println("Server check");
		logger.warn("Server check");
		return response;
	}
	
	@Override
	@GET
    @Path("/waittostart/{worklength}/{startingLat}/{startingLong}/")
	public SpootResponse waitToStart(@PathParam("worklength") int workLength, @PathParam("startingLat") double startingLat, @PathParam("startingLong") double startingLong) {
		System.out.println("Wait to start: " + startingLat + " " + startingLong);
		SpootResponse response = new SpootResponse();
		response.setInfo("waittostart");
		
		if(planningObj != null) {
			response.setContent("Planowanie w toku, nowe planowanie nie zosta�o uruchomione");
			response.setCode(1);
			return response;
		}
		response.setContent("Nowe planowanie zosta�o uruchomione");
		response.setCode(0);
		planningObj = new CmdApp();
		
		//planningObj = new CmdApp();
		if(workLength == 0) {
			planningObj.workLength = PropertiesValues.defaultWorkLength;
		} else {
			PropertiesValues.defaultWorkLength = workLength;
			planningObj.workLength = workLength;
		}
		
		planningObj.startingLat = startingLat;
		planningObj.startingLong = startingLong;
		waitingToStart = 1;
		//planning = new Thread(planningObj);
		//planning.start();
				
		return response;
	}
	
	@Override
	@GET
    @Path("/info")
	public SpootResponse info() {
		SpootResponse response = new SpootResponse();
		response.setInfo("info");
		if(planningObj != null) {
			response.setContent(planningObj.running == 1 ? "Planowanie w trakcie" : "Planowanie zako�czone/oczekuje na przewozy");
			response.setCode(0);
		} else {
			response.setContent("Planowanie wy��czone");
			response.setCode(1);
		}

		return response;
	}
	
	@Override
	@GET
    @Path("/finishnow")
	public SpootResponse finishNow() {
		SpootResponse response = new SpootResponse();
		response.setInfo("finishnow");
		if(planningObj == null) {
			response.setContent("Brak planowania do zatrzymania");
			response.setCode(1);
			return response;
		}
		planningObj.solver.terminateEarly();
		response.setContent("Planowanie zatrzymane");
		response.setCode(0);
		logger.warn("Finish now");
		return response;
	}
	
	@Override
	@GET
    @Path("/reset")
	public SpootResponse resetPlanning() {
		SpootResponse response = new SpootResponse();
		logger.warn("Reset");
		planning = null;
		waitingToStart = 0;
		pendingRides = new ArrayList<RideInformation>();
		approvedRides = new ArrayList<RideInformation>();
		cancelRides = new ArrayList<RideInformation>();
		rideNumber = 2;
		response.setInfo("reset");
		response.setContent("Planowanie zrestartowano");
		if(planningObj == null) {
			response.setCode(1);
			return response;
		}
		planningObj.solver.terminateEarly();
		planningObj = null;
		response.setCode(0);
		
		return response;
	}
	
	@Override
	@GET
    @Path("/addride/{pickupName}/{pickupLat}/{pickupLong}/{deliveryName}/{deliveryLat}/{deliveryLong}")
	public SpootResponse addRide(@PathParam("pickupName") String pickupName, @PathParam("pickupLat") double pickupLat, @PathParam("pickupLong") double pickupLong, @PathParam("deliveryName") String deliveryName, @PathParam("deliveryLat") double deliveryLat, @PathParam("deliveryLong") double deliveryLong) {		
		SpootResponse response = new SpootResponse();
		response.setInfo("addride");
		if(planningObj != null) {
			RideInformation ride = new RideInformation();
			ride.setRideName(pickupName + deliveryName);
			ride.setPickupName(pickupName);
			ride.setPickupLat(pickupLat);
			ride.setPickupLong(pickupLong);
			ride.setDeliveryName(deliveryName);
			ride.setDeliveryLat(deliveryLat);
			ride.setDeliveryLong(deliveryLong);
			ride.setRideNumber(rideNumber++);
			try {
				GeoApiContext context = new GeoApiContext.Builder().apiKey(PropertiesValues.geocodeApiKey).build();
				GeocodingResult[] results;
				com.google.maps.model.LatLng latlng = new com.google.maps.model.LatLng(pickupLat, pickupLong);
				results = GeocodingApi.newRequest(context).latlng(latlng).await();
				ride.setPickupAddress(results[0].formattedAddress);
				
				latlng = new com.google.maps.model.LatLng(deliveryLat, deliveryLong);
				results = GeocodingApi.newRequest(context).latlng(latlng).await();
				ride.setDeliveryAddress(results[0].formattedAddress);
				
			} catch (ApiException | InterruptedException | IOException e) {
				e.printStackTrace();
			}
			pendingRides.add(ride);
			response.setContent("Zg�oszenie czeka na akceptacje");
			logger.warn("New ride " + pickupName + " -> " + deliveryName);
			response.setCode(0);
			return response;
		} else {
			response.setContent("Brak aktywnego planowania");
			response.setCode(1);
			return response;
		}

	}

	@Override
	@GET
	@Path("/locklocation/{locationName}")
	public SpootResponse lockLocation(@PathParam("locationName") String locationName) {
		SpootResponse response = new SpootResponse();
		response.setInfo("locklocation");
		if(planningObj == null) {
			response.setContent("Lokalizacja nie zosta�a zablokowana, brak planownaia w toku");
			response.setCode(1);
			return response;
		}
		response.setContent("Lokalizacja " + locationName + " zsota�a zablokowana");
		planningObj.lockLocation(locationName);
		logger.warn("Lock loaction " + locationName);
		response.setCode(0);
		return response;
	}
	
	@Override
	@GET
	@Path("/hidelocation/{locationName}")
	public SpootResponse hideLocation(@PathParam("locationName") String locationName) {
		SpootResponse response = new SpootResponse();
		response.setInfo("hidelocation");
		if(planningObj == null) {
			response.setContent("Lokalizacja nie zosta�a ykryta, brak planownaia w toku");
			response.setCode(1);
			return response;
		}
		response.setContent("Lokalizacja " + locationName + " zsota�a ukryta");
		planningObj.hideLocation(locationName);
		planningObj.routeReady = true;
		logger.warn("Hide loaction " + locationName);
		response.setCode(0);
		return response;
	}
	
	@Override
	@GET
	@Path("/checkclientstatus/{rideName}")
	public SpootResponse checkClientStatus(@PathParam("rideName") String rideName) {
		SpootResponse response = new SpootResponse();
		response.setInfo("checkclientstatus");
		response.setCode(0);
		logger.warn("Check client status " + rideName);
		for(RideInformation ride : approvedRides) {
			if(ride.getRideName().equals(rideName)) {
				response.setContent("Zg�oszenie przyj�te.\n Przyjazd o " + planningObj.getClinetEta(rideName));
				return response;
			}
		}

		for(RideInformation ride : cancelRides) {
			if(ride.getRideName().equals(rideName)) {
				response.setContent("Zg�oszenie odrzucone");
				return response;
			}
		}
		
		for(RideInformation ride : pendingRides) {
			if(ride.getRideName().equals(rideName)) {
				response.setContent("Zg�oszenie czeka na akceptacje");
				return response;
			}
		}
		response.setContent("Zam�w przejazd");
		return response;
	}
	
	@Override
	@GET
	@Path("/cancelride/{rideName}")
	public SpootResponse cancelRide(@PathParam("rideName") String rideName) {
		SpootResponse response = new SpootResponse();
		response.setInfo("cancelRide");
		if(planningObj != null) {
			response.setContent("Przew�z " + rideName + " zosta� odrzucony");
			for(RideInformation ride : pendingRides) {
				if(ride.getRideName().equals(rideName)) {
					pendingRides.remove(ride);
					cancelRides.add(ride);
					response.setCode(0);
					return response;
					
				}
			}
		}
		response.setCode(1);
		response.setContent("Brak aktywnego planowania");
		logger.warn("Cancel ride " + rideName);
		return response;
	}
	
	@Override
	@GET
	@Path("/getstops")
	public SpootResponse sendStops() {
		SpootResponse response = new SpootResponse();
		response.setInfo("stops");
		logger.warn("Get stops");
		if(planningObj != null) {
			List<StopInformation> stops = new ArrayList<StopInformation>();
			RoutingSolution solution = planningObj.solver.getBestSolution();
			
			Visit visit = solution.getVehicleList().get(0).getNextVisit();
			while(visit != null) {
				if(!visit.getLocation().getDriverArrived()) {
					StopInformation stopInfo = new StopInformation();
					stopInfo.setLatitude(visit.getLocation().getLatitude());
					stopInfo.setLongtitude(visit.getLocation().getLongitude());
					stopInfo.setName(visit.getLocation().getName());
					stopInfo.setDelivery(visit.getVisitType() == VisitType.PICKUP ? false : true);
					stopInfo.setAdditionalLocation(visit.getVisitType() == VisitType.PICKUP ? visit.getRide().getDeliveryVisit().getLocation().getName() : visit.getRide().getPickupVisit().getLocation().getName());
					stopInfo.setAddress(visit.getAddress());
					stops.add(stopInfo);
				}
				visit = visit.getNextVisit();
			}

			if(stops.isEmpty()) stops = null;
			response.setContent(stops);
			return response;
		} else {
			response.setCode(1);
			response.setContent(null);
			return response;
		}
		
	}
	
	@Override
	@GET
	@Path("/getpendingrides")
	public SpootResponse getPendingList(){
		SpootResponse response = new SpootResponse();
		response.setInfo("pendingRides");
		if(planningObj != null) {
			response.setContent(pendingRides);
			response.setRouteReady(planningObj.routeReady);
			response.setCode(0);
			if(planningObj.routeReady) {
				planningObj.routeReady = false;
			}
		}else {
			response.setCode(1);
			response.setContent(null);
		}

		logger.warn("Get pending rides");
		return response;
	}
	
	@Override
	@GET
	@Path("/approveride/{rideName}")
	public SpootResponse approveRide(@PathParam("rideName") String rideName){
		SpootResponse response = new SpootResponse();
		response.setInfo("approveRide");
		logger.warn("Approve ride " + rideName);
		if(planningObj != null) {
			response.setContent("Przew�z " + rideName + " zatwierdzony");
			for(RideInformation ride : pendingRides) {
				if(ride.getRideName().equals(rideName)) {
					if(waitingToStart == 1) {
						planningObj.pickupLat = ride.getPickupLat();
						planningObj.pickupLong = ride.getPickupLong();
						planningObj.deliveryLat = ride.getDeliveryLat();
						planningObj.deliveryLong = ride.getDeliveryLong();
						planningObj.pickupName = ride.getPickupName();
						planningObj.deliveryName = ride.getDeliveryName();
						planningObj.pickupAddress = ride.getPickupAddress();
						planningObj.deliveryAddress = ride.getDeliveryAddress();
						planning = new Thread(planningObj);
						planning.start();						
						waitingToStart = 2;
						pendingRides.remove(ride);
						approvedRides.add(ride);
						response.setCode(0);
						return response;
					}else {
						planningObj.addRideInformation(ride);
						pendingRides.remove(ride);
						approvedRides.add(ride);
						response.setCode(0);
						return response;
					}
				}
			}
			
			response.setCode(1);
			response.setContent("Nie znaleziono przewozu " + rideName);
			return response;
		} else {
			response.setContent("Brak aktywnego planowania");
			response.setCode(1);
			return response;
		}
	}
	
	@Override
	@GET
	@Path("/generaterandomride")
	public SpootResponse generateRandomRide() {
		SpootResponse response = new SpootResponse();
		response.setInfo("generaterandomride");
		logger.warn("Generate random ride");
		if(planningObj != null) {
			LatLng centerLoc = new LatLng(PropertiesValues.startRandomLocLat, PropertiesValues.startRandomLocLng);
			LatLng from = generateRandomPoint(centerLoc, PropertiesValues.RandomRadius);
			LatLng to = generateRandomPoint(centerLoc, PropertiesValues.RandomRadius);
		    int length = PropertiesValues.randomClientNameLength;
		    String clinetNmae = RandomStringUtils.random(length, true, true);
		    addRide(clinetNmae + "_PU", from.getLat(), from.getLng(), clinetNmae + "_DE", to.getLat(), to.getLng());
		    response.setCode(0);
		    response.setContent(clinetNmae + "_PU " + from.getLat() + "; " + from.getLng() + " | " + clinetNmae + "_DE " + to.getLat() + "; " + to.getLng());
		    return response;
		} else {
			response.setCode(1);
			response.setContent("Brak aktywnego planowania");
			return response;
		}
	}
	
	@Override
	@GET
	@Path("/generaterandomrideaccpet")
	public SpootResponse generateRandomRideAccept() {
		SpootResponse response = new SpootResponse();
		response.setInfo("generaterandomride");
		logger.warn("Generate random ride");
		if(planningObj != null) {
			
			LatLng centerLoc = new LatLng(PropertiesValues.startRandomLocLat, PropertiesValues.startRandomLocLng);
			LatLng from = generateRandomPoint(centerLoc, PropertiesValues.RandomRadius);
			LatLng to = generateRandomPoint(centerLoc, PropertiesValues.RandomRadius);
		    int length = PropertiesValues.randomClientNameLength;
		    String clinetNmae = RandomStringUtils.random(length, true, true);
		    addRide(clinetNmae + "_PU", from.getLat(), from.getLng(), clinetNmae + "_DE", to.getLat(), to.getLng());
		    approveRide(clinetNmae + "_PU" + clinetNmae + "_DE");
		    response.setCode(0);
		    response.setContent(clinetNmae + "_PU " + from.getLat() + "; " + from.getLng() + " | " + clinetNmae + "_DE " + to.getLat() + "; " + to.getLng());
			return response;
		} else {
			response.setCode(1);
			response.setContent("Brak aktywnego planowania");
			return response;
		}

	}
	
	public LatLng generateRandomPoint(LatLng center, int radius) {
	    Random random = new Random();

	    double radiusInDegrees = radius / 111000f;

	    double u = random.nextDouble();
	    double v = random.nextDouble();
	    double w = radiusInDegrees * Math.sqrt(u);
	    double t = 2 * Math.PI * v;
	    double x = w * Math.cos(t);
	    double y = w * Math.sin(t);

	    double new_x = x / Math.cos(Math.toRadians(center.getLat()));

	    double foundLongitude = new_x + center.getLng();
	    double foundLatitude = y + center.getLat();
	    return new LatLng(foundLatitude, foundLongitude);
	}

}
