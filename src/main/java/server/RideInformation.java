package server;

public class RideInformation {
	
	private int rideNumber;
	private String rideName;
	private String deliveryName;
	private String pickupName;
	private double pickupLat;
	private double pickupLong;
	private double deliveryLat;
	private double deliveryLong;
	private String pickupAddress;
	private String deliveryAddress;
	private String eta;
	
	public String getEta() {
		return eta;
	}
	
	public void setEta(String eta) {
		this.eta = eta;
	}
	
	public String getPickupAddress() {
		return pickupAddress;
	}
	
	public void setPickupAddress(String pickupAddress) {
		this.pickupAddress = pickupAddress;
	}
	
	public String getDeliveryAddress() {
		return deliveryAddress;
	}
	
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	
	public int getRideNumber() {
		return rideNumber;
	}
	
	public void setRideNumber(int rideNumber) {
		this.rideNumber = rideNumber;
	}
	
	public String getRideName() {
		return rideName;
	}
	
	public void setRideName(String rideName) {
		this.rideName = rideName;
	}
	
	public String getDeliveryName() {
		return deliveryName;
	}
	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}
	public String getPickupName() {
		return pickupName;
	}
	public void setPickupName(String pickupName) {
		this.pickupName = pickupName;
	}
	public double getPickupLat() {
		return pickupLat;
	}
	public void setPickupLat(double pickupLat) {
		this.pickupLat = pickupLat;
	}
	public double getPickupLong() {
		return pickupLong;
	}
	public void setPickupLong(double pickupLong) {
		this.pickupLong = pickupLong;
	}
	public double getDeliveryLat() {
		return deliveryLat;
	}
	public void setDeliveryLat(double deliveryLat) {
		this.deliveryLat = deliveryLat;
	}
	public double getDeliveryLong() {
		return deliveryLong;
	}
	public void setDeliveryLong(double deliveryLong) {
		this.deliveryLong = deliveryLong;
	}

}
