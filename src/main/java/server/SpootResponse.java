package server;

public class SpootResponse {
	private String info;
	private Object content;
	private boolean routeReady;
	private int code;
	
	public int getCode() {
		return code;
	}
	
	public void setCode(int code) {
		this.code = code;
	}
	
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public Object getContent() {
		return content;
	}
	public void setContent(Object content) {
		this.content = content;
	}
	
	public boolean getRouteReady() {
		return routeReady;
	}
	
	public void setRouteReady(boolean routeReady) {
		this.routeReady = routeReady;
	}
	
}
