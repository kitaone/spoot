package server;

public class StopInformation {
	private double latitude;
	private double longtitude;
	private String name;
	private boolean delivery;
	private String additionalLocation;
	private String address;
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getAdditionalLocation() {
		return additionalLocation;
	}
	
	public void setAdditionalLocation(String additionalLocation) {
		this.additionalLocation = additionalLocation;
	}
	
	public boolean getDelivery() {
		return delivery;
	}
	
	public void setDelivery(boolean delivery) {
		this.delivery = delivery;
	}
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongtitude() {
		return longtitude;
	}
	public void setLongtitude(double longtitude) {
		this.longtitude = longtitude;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
