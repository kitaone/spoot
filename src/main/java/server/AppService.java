package server;

import java.util.List;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

public interface AppService {
	
	public SpootResponse areUAlive();
	
	public SpootResponse waitToStart(int workLength, double startingLat, double startingLong);
	
	public SpootResponse info();
	
	public SpootResponse finishNow();
	
	public SpootResponse addRide(String pickupName, double pickupLat, double pickupLong, String deliveryName, double deliveryLat, double deliveryLong);
	
	public SpootResponse lockLocation(String locationName);
	
	public SpootResponse hideLocation(String locationName);
	
	//public List<StopInformation> sendStops();
	
	public SpootResponse sendStops();
	
	public SpootResponse getPendingList();
	
	public SpootResponse approveRide(String ride);
	
	public SpootResponse checkClientStatus(String clientName);
	
	public SpootResponse cancelRide(String rideName);
	
	public SpootResponse generateRandomRide();
	
	public SpootResponse generateRandomRideAccept();
	
	public SpootResponse resetPlanning();

}
